package com.safari.rhinodestruction;

public interface ObservadorImagenesRhino 
{
    void sequenceEnded(String imageName);
}

