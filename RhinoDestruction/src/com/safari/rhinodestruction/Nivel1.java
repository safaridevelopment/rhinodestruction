package com.safari.rhinodestruction;

public class Nivel1 {
	// RHINO PANEL
	private String IMAGENES_INFO = "imsInfo_nivel1.txt";
	private String LADRILLOS_INFO = "bricksInfo_nivel1.txt";
	private int VIDA = 100;
	private int PUNT_MAX = 300;
	private int vidarest = VIDA;
	// LADRILLOS
	private double MOVE_FACTOR = 0.2;
	// LAZO
	private String fondo1 = "fondo1";
	private String fondo2 = "fondo2";
	private String fondo3 = "fondo3";
	// MUSICAA
	private String musica = "Sonidos/rhino.wav";

	public Nivel1() {
	}

	public String getImagenes_Info() {
		return IMAGENES_INFO;
	}

	public String getLadrillos_Info() {
		return LADRILLOS_INFO;
	}

	public int getVida() {
		return VIDA;
	}

	public int getPunt_Max() {
		return PUNT_MAX;
	}

	public int getVidarest() {
		return vidarest;
	}

	public double getMove_Factor() {
		return MOVE_FACTOR;
	}

	public String getFondo1() {
		return fondo1;
	}

	public String getFondo2() {
		return fondo2;
	}

	public String getFondo3() {
		return fondo3;
	}

	public String getMusica() {
		return musica;
	}
}
