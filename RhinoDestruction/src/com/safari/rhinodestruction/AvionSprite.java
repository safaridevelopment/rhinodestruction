package com.safari.rhinodestruction;

import java.awt.*;

public class AvionSprite extends Sprite {
	private static final int PASO = -8;
	private static final int STEP_OFFSET = 2;
	private RhinoPanel rp;
	private RhinoSprite rhino;

	public AvionSprite(int w, int h, CargadorDeImg imsLd, RhinoPanel rp,
			RhinoSprite r) {
		super(w, h / 2, w, h, imsLd, "avion");
		this.rp = rp;
		rhino = r;
		posicionInicial();
	}

	private void posicionInicial() {
		int h = 20;
		if (h + getHeight() > getPHeight() - 40)
			h -= getHeight();
		setPosicion(getPWidth() + 5, h + 5);
		setPaso(PASO + getRandRange(STEP_OFFSET), 0);
	}

	private int getRandRange(int x) {
		return ((int) (2 * x * Math.random())) - x;
	}

	public void updateAvionSprite() {
		golpeoRhino();
		fueraDePantalla();
		super.updateRhinoSprite();
	}

	private void golpeoRhino() {
		Rectangle rectRhino = rhino.getMiRectangulo();
		rectRhino.grow(-rectRhino.width / 3, 0);
		if (rectRhino.intersects(getMiRectangulo())) {
			rp.destruirAvion(posX, posY + getHeight() / 2);
			posicionInicial();
		}
	}

	private void fueraDePantalla() {
		if (((posX + getWidth()) <= 0) && (dx < 0))
			posicionInicial();
	}

}