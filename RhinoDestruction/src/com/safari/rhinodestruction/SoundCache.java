package com.safari.rhinodestruction;

import java.io.File;
import java.util.HashMap;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class SoundCache {
	private static SoundCache INSTANCE = null;
	HashMap<String, Clip> clips = new HashMap<String, Clip>();

	public SoundCache() {
	}

	public Clip get(String filename) {
		if (clips.get(filename) == null) {
			File file = new File(filename);
			Clip audioClip = null;
			AudioInputStream audioInputStream = null;
			try {
				audioInputStream = AudioSystem.getAudioInputStream(file);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (audioInputStream != null) {
				AudioFormat format = audioInputStream.getFormat();
				DataLine.Info info = new DataLine.Info(Clip.class, format,
						AudioSystem.NOT_SPECIFIED);
				try {
					audioClip = (Clip) AudioSystem.getLine(info);
					audioClip.open(audioInputStream);
					audioClip.drain();
					clips.put(filename, audioClip);
				} catch (Exception e) {
					e.printStackTrace();
					audioClip = null;
				}
			}
		}
		return clips.get(filename);
	}

	private synchronized static void createInstance() {
		if (INSTANCE == null) {
			INSTANCE = new SoundCache();
		}
	}

	public static SoundCache getInstance() {
		createInstance();
		return INSTANCE;
	}

	public static void play(Clip clip) {
		if (clip != null) {
			clip.stop();
			clip.flush();
			clip.setFramePosition(0);
			clip.loop(1);
		}
	}

	public static void stop(Clip clip) {
		if (clip != null)
			clip.stop();
	}
}
