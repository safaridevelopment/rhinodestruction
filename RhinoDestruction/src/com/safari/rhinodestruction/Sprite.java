package com.safari.rhinodestruction;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Sprite {
	private static final int PASOX = 5;
	private static final int PASOY = 5;
	private static final int TAMANO = 12;
	private CargadorDeImg carImg;
	private String nombreImagen;
	private BufferedImage imagen;
	private int width, height;
	private ImagenesRhino player;
	private boolean isLooping;
	private int pWidth, pHeight;
	private boolean estaActivo = true;
	protected int posX, posY;
	protected int dx, dy;

	public Sprite(int x, int y, int w, int h, CargadorDeImg cImg, String nombre) {
		posX = x;
		posY = y;
		pWidth = w;
		pHeight = h;
		dx = PASOX;
		dy = PASOY;
		carImg = cImg;
		setImage(nombre);
	}

	public void setImage(String nombre) {
		nombreImagen = nombre;
		imagen = carImg.getImg(nombreImagen);
		if (imagen == null) {
			System.out.println("No hay imagen para " + nombreImagen);
			width = TAMANO;
			height = TAMANO;
		} else {
			width = imagen.getWidth();
			height = imagen.getHeight();
		}
		player = null;
		isLooping = false;
	}

	public void loopImage(int periodoAnim, double duracionSec) {
		if (carImg.numImages(nombreImagen) > 1) {
			player = null;
			player = new ImagenesRhino(nombreImagen, periodoAnim, duracionSec,
					true, carImg);
			isLooping = true;
		} else
			System.out.println(nombreImagen
					+ " no es una secuencia de imagenes");
	}

	public void stopLooping() {
		if (isLooping) {
			player.stop();
			isLooping = false;
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getPWidth() {
		return pWidth;
	}

	public int getPHeight() {
		return pHeight;
	}

	public boolean estaActivo() {
		return estaActivo;
	}

	public void setEstaActivo(boolean a) {
		estaActivo = a;
	}

	public void setPosicion(int x, int y) {
		posX = x;
		posY = y;
	}

	public void trasladar(int distX, int distY) {
		posX += distX;
		posY += distY;
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPaso(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	public int getPasoX() {
		return dx;
	}

	public int getPasoY() {
		return dy;
	}

	public Rectangle getMiRectangulo() {
		return new Rectangle(posX, posY, width, height);
	}

	public void updateRhinoSprite() {
		if (estaActivo()) {
			posX += dx;
			posY += dy;
			if (isLooping)
				player.updateTick();
		}
	}

	public void dibujarSprite(Graphics g) {
		if (estaActivo()) {
			if (imagen == null) {
				g.setColor(Color.yellow);
				g.fillOval(posX, posY, TAMANO, TAMANO);
				g.setColor(Color.black);
			} else {
				if (isLooping)
					imagen = player.getCurrentImage();
				g.drawImage(imagen, posX, posY, null);
			}
		}
	}
}