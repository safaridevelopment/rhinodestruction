package com.safari.rhinodestruction;

public class Nivel5 {
	// RHINO PANEL
	private String IMAGENES_INFO = "imsInfo_nivel5.txt";
	private String LADRILLOS_INFO = "bricksInfo_nivel5.txt";
	private int VIDA = 100;
	private int PUNT_MAX = 500;
	private int vidarest = VIDA;
	// LADRILLOS
	private double MOVE_FACTOR = 0.2;
	// MISILES
	private int PASO = -11;
	private int PASO2 = -10;
	// LAZO
	private String fondo1 = "fondo1_nivel5";
	private String fondo2 = "fondo2_nivel5";
	private String fondo3 = "fondo3_nivel5";
	// MUSICAA
	private String musica = "Sonidos/puta.wav";

	public Nivel5() {
	}

	public String getImagenes_Info() {
		return IMAGENES_INFO;
	}

	public String getLadrillos_Info() {
		return LADRILLOS_INFO;
	}

	public int getVida() {
		return VIDA;
	}

	public int getPunt_Max() {
		return PUNT_MAX;
	}

	public int getVidarest() {
		return vidarest;
	}

	public double getMove_Factor() {
		return MOVE_FACTOR;
	}

	public int getPasoMisil() {
		return PASO;
	}

	public int getPasoMisil2() {
		return PASO2;
	}

	public String getFondo1() {
		return fondo1;
	}

	public String getFondo2() {
		return fondo2;
	}

	public String getFondo3() {
		return fondo3;
	}

	public String getMusica() {
		return musica;
	}
}
