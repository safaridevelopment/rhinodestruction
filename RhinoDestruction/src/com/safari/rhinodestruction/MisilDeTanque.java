package com.safari.rhinodestruction;

import java.awt.*;

public class MisilDeTanque extends Sprite {
	private static final int PASO = 18;
	private static final int STEP_OFFSET = 2;
	private RhinoPanel rp;
	private RhinoSprite rhino;
	private TankSprite tanque;

	public MisilDeTanque(int w, int h, CargadorDeImg imsLd, RhinoPanel rp,
			RhinoSprite r, TankSprite tank) {
		super(w, h / 2, w, h, imsLd, "misildetanque");
		this.rp = rp;
		rhino = r;
		tanque = tank;
		posicionInicial();
	}

	private void posicionInicial() {
		setPosicion(tanque.getPosX() + tanque.getWidth(),
				560 - tanque.getHeight());
		setPaso(PASO + getRandRange(STEP_OFFSET), 0);
	}

	private int getRandRange(int x) {
		return ((int) (2 * x * Math.random())) - x;
	}

	public void updateMisilDeTanque() {
		golpeoRhino();
		fueraDePantalla();
		super.updateRhinoSprite();
	}

	private void golpeoRhino() {
		Rectangle rectRhino = rhino.getMiRectangulo();
		rectRhino.grow(-rectRhino.width / 3, 0);
		if (rectRhino.intersects(getMiRectangulo())) {
			rp.explosion(posX, posY + getHeight() / 2);
			posicionInicial();
			rp.vida += 5;
		}
	}

	private void fueraDePantalla() {
		if (((-posX + getWidth() * 40) <= 0) && (dx > 0))
			posicionInicial();
	}
}