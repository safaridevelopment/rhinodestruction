package com.safari.rhinodestruction;

public class TankSprite extends Sprite {
	private static final int PASO = 8;
	private static final int STEP_OFFSET = 2;

	public TankSprite(int w, int h, CargadorDeImg imsLd, RhinoPanel rp,
			RhinoSprite r) {
		super(w, h / 2, w, h, imsLd, "tank");
		posicionInicial();
	}

	private void posicionInicial() {
		int h = 505;
		if (h + getHeight() > getPHeight() - 40)
			h -= getHeight();
		setPosicion(-getWidth(), h);
		setPaso(PASO + getRandRange(STEP_OFFSET), 0);
	}

	private int getRandRange(int x) {
		return ((int) (2 * x * Math.random())) - x;
	}

	public void updateTankSprite() {
		fueraDePantalla();
		super.updateRhinoSprite();
	}

	private void fueraDePantalla() {
		if (((-posX + getWidth() * 15) <= 0) && (dx > 0))
			posicionInicial();
	}
}