package com.safari.rhinodestruction;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class MenuCanvas extends JPanel {
	private static final long serialVersionUID = 1L;
	Graphics2D g2d;
	private boolean instrucciones;
	private boolean informacion;
	private BufferedImage logoImg;
	private BufferedImage instruccionesImg;
	private BufferedImage informacionImg;

	public MenuCanvas() {
		try {
			logoImg = ImageIO.read(ClassLoader
					.getSystemResourceAsStream("Images/RhinoLogo.png"));
			instruccionesImg = ImageIO.read(ClassLoader
					.getSystemResourceAsStream("Images/instrucciones.jpg"));
			informacionImg = ImageIO.read(ClassLoader
					.getSystemResourceAsStream("Images/informacion.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);
		update(g);
	}

	public void update(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(logoImg, 10, 10, null);
		if (instrucciones && !informacion) {
			g2d.drawImage(instruccionesImg, 10, 10, null);
		}
		if (informacion && !instrucciones) {
			g2d.drawImage(informacionImg, 10, 10, null);
		}
	}

	public void publicarInstrucciones() {
		if (!instrucciones) {
			instrucciones = true;
			informacion = false;
			repaint();
		} else {
			instrucciones = false;
			repaint();
		}
	}

	public void publicarInformacion() {
		if (!informacion) {
			informacion = true;
			instrucciones = false;
			repaint();
		} else {
			informacion = false;
			repaint();
		}
	}
}