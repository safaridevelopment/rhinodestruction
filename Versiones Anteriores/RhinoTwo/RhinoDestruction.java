import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RhinoDestruction extends JFrame 
{
  private static int DEFAULT_FPS = 30;

  private RhinoPanel jp;

  public RhinoDestruction(long period)
  { super("RhinoDestruction");

    Container c = getContentPane();
    jp = new RhinoPanel(this, period);
    c.add(jp, "Center");

    pack();
    setResizable(false);
    setVisible(true);
  } 
 
  public static void main(String args[])
  { 
    long period = (long) 1000.0/DEFAULT_FPS;
    new RhinoDestruction(period*1000000L);
  }
}
