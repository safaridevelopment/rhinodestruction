import java.awt.Graphics;
import java.awt.image.BufferedImage;


public class Lazo
{
  private BufferedImage im;
  private int ancho;      
  private int pAncho, pAlto;    

  private int movTamanno;       
  private boolean seMueveDerecha;  
  private boolean seMueveIzquierda;

  private int xInicio;   


  public Lazo(int w, int h, BufferedImage im, int moveSz)
  {
    pAncho = w; pAlto = h;
    
    this.im = im;
    ancho = im.getWidth();    
    if (ancho < pAncho) 
      System.out.println("problema con el ancho del panel");

    movTamanno = moveSz;
    seMueveDerecha = false;  
    seMueveIzquierda = false;
    xInicio = 0;
  }  

  public void moverHaciaDerecha()
  { seMueveDerecha = true;
    seMueveIzquierda = false;
  }

  public void moverHaciaIzquierda()
  { seMueveDerecha = false;
    seMueveIzquierda = true;
  }

  public void sinMovimiento()
  { seMueveDerecha = false;
    seMueveIzquierda = false;
  }
  
  public void actualizar()
  { if (seMueveDerecha)
      xInicio = (xInicio + movTamanno) % ancho;
    else if (seMueveIzquierda)
      xInicio = (xInicio - movTamanno) % ancho;
  } 

  public void pantalla(Graphics g)
  {
    if (xInicio == 0) 
      dibujar(g, im, 0, pAncho, 0, pAncho);
    else if ((xInicio > 0) && (xInicio < pAncho)) {   
      dibujar(g, im, 0, xInicio, ancho-xInicio, ancho);  
      dibujar(g, im, xInicio, pAncho, 0, pAncho-xInicio);
    }
    else if (xInicio >= pAncho) 
      dibujar(g, im, 0, pAncho, 
                  ancho-xInicio, ancho-xInicio+pAncho); 
    else if ((xInicio < 0) && (xInicio >= pAncho-ancho))
      dibujar(g, im, 0, pAncho, -xInicio, pAncho-xInicio); 
    else if (xInicio < pAncho-ancho) {
      dibujar(g, im, 0, ancho+xInicio, -xInicio, ancho);
      dibujar(g, im, ancho+xInicio, pAncho, 
                  0, pAncho-ancho-xInicio); 
    }
  } 


  private void dibujar(Graphics g, BufferedImage im, 
                        int scrX1, int scrX2, int imX1, int imX2)
  { g.drawImage(im, scrX1, 0, scrX2, pAlto, 
                     imX1, 0,  imX2, pAlto, null);
  }

}
