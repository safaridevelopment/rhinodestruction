import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

import com.sun.j3d.utils.timer.J3DTimer;

public class RhinoPanel extends JPanel implements Runnable {
	private static final int PANEL_WIDTH = 800;
	private static final int PANEL_HEIGHT = 600;

	private static final int NO_DELAYS_PER_YIELD = 16;

	private static final int MAX_FRAME_SKIPS = 5;
	private static final int PUNT_MAX = 1000;
	private static final String IMAGENES_INFO = "imsInfo.txt";
	private static final String LADRILLOS_INFO = "bricksInfo.txt";

	private Thread animator;
	private volatile boolean running = false;
	private boolean gameOver = false;

	private long periodo;

	private int tiempoEnJuego;
	private int puntaje = 0;

	private Font msgFont, msgFontGO;
	private FontMetrics metrics;

	private RhinoDestruction rhinoTop;

	private RhinoSprite rhino;

	private ManejadorDeLazo lazin;
	private ManejadorDeLadrillos ladrillin;

	private long gameStartTime, iniciaTiempoEnJuego;

	private Graphics dbg;
	private Image dbImage = null;

	public RhinoPanel(RhinoDestruction jj, long period) {
		rhinoTop = jj;
		this.periodo = period;

		setDoubleBuffered(false);
		setBackground(Color.white);
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

		setFocusable(true);
		requestFocus();
		addKeyListener(keyl);

		CargadorDeImg imsLoader = new CargadorDeImg(IMAGENES_INFO);

		ladrillin = new ManejadorDeLadrillos(PANEL_WIDTH, PANEL_HEIGHT,
				LADRILLOS_INFO, imsLoader);
		int brickMoveSize = ladrillin.getVelocidad();

		lazin = new ManejadorDeLazo(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
				imsLoader);

		rhino = new RhinoSprite(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
				ladrillin, imsLoader, (int) (period / 1000000L));

		msgFont = new Font("SansSerif", Font.BOLD, 15);
		msgFontGO = new Font("SansSerif", Font.BOLD, 18);
		metrics = this.getFontMetrics(msgFont);
	}

	KeyListener keyl = new KeyListener() {
		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				rhino.sinMovimiento();
				ladrillin.quieto();
				lazin.moverNo();
				break;
			case KeyEvent.VK_RIGHT:
				rhino.sinMovimiento();
				ladrillin.quieto();
				lazin.moverNo();
				break;
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				rhino.movIzquierda();
				ladrillin.moverDerecha();
				lazin.moverDerecha();
				break;
			case KeyEvent.VK_RIGHT:
				rhino.movDerecha();
				ladrillin.moverIzquierda();
				lazin.moverIzquierda();
				break;
			case KeyEvent.VK_UP:
				rhino.salto();
				break;
			case KeyEvent.VK_SPACE:
				rhino.golpe();
				break;
			case KeyEvent.VK_ESCAPE:
				if (gameOver)
					stopGame();
				break;
			case KeyEvent.VK_R:
				break;
			}
		}
	};

	public void addNotify() {
		super.addNotify();
		startGame();
	}

	private void startGame() {
		if (animator == null || !running) {
			animator = new Thread(this);
			animator.start();
		}
	}

	public void stopGame() {
		running = false;
	}

	public void run() {
		long beforeTime, afterTime, timeDiff, sleepTime;
		long overSleepTime = 0L;
		int noDelays = 0;
		long excess = 0L;

		gameStartTime = J3DTimer.getValue();
		// gameStartTime = System.currentTimeMillis();
		iniciaTiempoEnJuego = System.currentTimeMillis();
		beforeTime = gameStartTime;

		running = true;

		while (running) {
			gameUpdate();
			gameRender();
			paintScreen();

			afterTime = J3DTimer.getValue();
			timeDiff = afterTime - beforeTime;
			sleepTime = (periodo - timeDiff) - overSleepTime;

			if (sleepTime > 0) {
				try {
					Thread.sleep(sleepTime / 100000000L);
				} catch (InterruptedException ex) {
				}
				overSleepTime = (J3DTimer.getValue() - afterTime) - sleepTime;
			} else {
				excess -= sleepTime;
				overSleepTime = 0L;

				if (++noDelays >= NO_DELAYS_PER_YIELD) {
					Thread.yield();
					noDelays = 0;
				}
			}

			beforeTime = J3DTimer.getValue();
			int skips = 0;
			while ((excess > periodo) && (skips < MAX_FRAME_SKIPS)) {
				excess -= periodo;
				gameUpdate();
				skips++;
			}
		}
		System.exit(0);
	}

	private void gameUpdate() {
		if (!gameOver) {
			if (rhino.willHitBrick()) {
				rhino.sinMovimiento();
				ladrillin.quieto();
				lazin.moverNo();
			}
			lazin.actualizar();
			ladrillin.update();
			rhino.updateRhinoSprite();
		}
	}

	private void gameRender() {
		if (dbImage == null) {
			dbImage = createImage(PANEL_WIDTH, PANEL_HEIGHT);
			if (dbImage == null) {
				System.out.println("dbImage is null");
				return;
			} else
				dbg = dbImage.getGraphics();
		}

		dbg.setColor(Color.white);
		dbg.fillRect(0, 0, PANEL_WIDTH, PANEL_HEIGHT);

		lazin.display(dbg);
		ladrillin.display(dbg);
		rhino.dibujarSprite(dbg);

		contadorTiempo(dbg);
		if (gameOver)
			mensajeGameOver(dbg);
	}

	private void paintScreen() {
		Graphics g;
		try {
			g = this.getGraphics();
			if ((g != null) && (dbImage != null))
				g.drawImage(dbImage, 0, 0, null);

			Toolkit.getDefaultToolkit().sync();
			g.dispose();
		} catch (Exception e) {
			System.out.println("Error de Contexto Grafico: " + e);
		}
	}

	private void contadorTiempo(Graphics g) {
		if (!gameOver)
			tiempoEnJuego = (int) ((System.currentTimeMillis() - iniciaTiempoEnJuego) / 1000L);
		if (tiempoEnJuego == 120)
			gameOver = true;
		g.setColor(Color.white);
		g.setFont(msgFont);
		g.drawString("Tiempo Restante: " + (120 - tiempoEnJuego) + " segs", 10,
				20);
		g.drawString("Puntaje: " + puntaje + "/" + PUNT_MAX, 10, 40);
	}

	private void mensajeGameOver(Graphics g) {
		String msg = "Juego Terminado. Se ha acabado el tiempo.";
		String msg2 = "Pulse ESC para salir.";
		int x = (PANEL_WIDTH - metrics.stringWidth(msg)) / 2;
		int x1 = (PANEL_WIDTH - metrics.stringWidth(msg2)) / 2;
		int y = (PANEL_HEIGHT - metrics.getHeight()) / 3;
		g.setColor(Color.white);
		g.setFont(msgFontGO);
		g.drawString(msg, x, y);
		g.drawString(msg2, x1, y + 18);
	}
}