//import com.safari.rhino.ManejadorSonidos;

public class RhinoSprite extends Sprite {
	private static double DURACION = 0.5;
	private static final int SIN_SALTO = 0;
	private static final int SUBIENDO = 1;
	private static final int BAJANDO = 2;

	private static final int DIST_MAX_SUBIENDO = 8;

	private int periodo;

	public boolean derecha, quieto, golpe;

	private int movVertical;
	private int distVertical;
	private int contSubiendo;

	private ManejadorDeLadrillos ladrillin;
	private int velocidad;

	// /Sonidos
//	ManejadorDeSonido sonidos = new ManejadorDeSonido();

	private int xMundo, yMundo;

	public RhinoSprite(int w, int h, int velocidadLadrillo,
			ManejadorDeLadrillos ml, CargadorDeImg carImg, int p) {
		super(w/2, h / 2, w, h, carImg, "quietoderecha");

		velocidad = velocidadLadrillo;

		ladrillin = ml;
		periodo = p;
		setPaso(0, 0);

		derecha = true;
		quieto = true;
		golpe = false;

		posY = ladrillin.encontrarPiso(posX + getWidth() / 2) - getHeight();
		xMundo = posX;
		yMundo = posY;

		movVertical = SIN_SALTO;
		distVertical = ladrillin.getLadrilloHeight() / 2;
		contSubiendo = 0;
	}

	public void movIzquierda() {
		setImage("izquierda");
		loopImage(periodo, DURACION);
		derecha = false;
		quieto = false;
//		sonidos.sonido("step1.wav");
	}

	public void movDerecha() {
		setImage("derecha");
		loopImage(periodo, DURACION);
		derecha = true;
		quieto = false;
//		sonidos.sonido("step1.wav");
	}

	public void sinMovimiento() {
		if(derecha){
			setImage("quietoderecha");
			loopImage(periodo, DURACION);
		}
		else
			setImage("stilltotalizq");
		stopLooping();
		quieto = true;
	}

	public void golpe() {
		if (derecha) {
			setImage("punchtotalder");
			//loopImage(periodo, DURACION);
			
			
		} else {
			setImage("punchtotalizq");
			//loopImage(periodo, DURACION);
		}
		golpe = true;
		
	}

	public void salto() {
		if (movVertical == SIN_SALTO) {
			movVertical = SUBIENDO;
			contSubiendo = 0;
			if (quieto) {
				if (derecha)
					setImage("saltoderecha");
				else
					setImage("saltoizquierda");
			}
		}
	}

	public boolean willHitBrick() {
		if (quieto)
			return false;
		int testX;
		if (derecha)
			testX = xMundo + velocidad;
		else
			testX = xMundo - velocidad;
		int xMid = testX + getWidth() / 2;
		int yMid = yMundo + (int) (getHeight() * 0.8);

		return ladrillin.dentroDeLadrillo(xMid, yMid);
	}

	public void updateRhinoSprite() {
		if (!quieto) {
			if (derecha)
				xMundo += velocidad;
			else
				xMundo -= velocidad;
			if (movVertical == SIN_SALTO)
				checkSiCae();
		}
		if (movVertical == SUBIENDO)
			updateSubiendo();
		else if (movVertical == BAJANDO)
			updateCayendo();

		super.updateRhinoSprite();
	}

	private void checkSiCae() {
		int yTrans = ladrillin.checkTopeLadrillo(xMundo + (getWidth() / 2),
				yMundo + getHeight() + distVertical, distVertical);
		if (yTrans != 0)
			movVertical = BAJANDO;
	}

	private void updateSubiendo() {
		if (contSubiendo == DIST_MAX_SUBIENDO) {
			movVertical = BAJANDO;
			contSubiendo = 0;
		} else {
			int yTrans = ladrillin.checkBaseLadrillo(xMundo + (getWidth() / 2),
					yMundo - distVertical, distVertical);
			if (yTrans == 0) {
				movVertical = BAJANDO;
				contSubiendo = 0;
			} else {
				trasladar(0, -yTrans);
				yMundo -= yTrans;
				contSubiendo++;
			}
		}
	}

	private void updateCayendo() {
		int yTrans = ladrillin.checkTopeLadrillo(xMundo + (getWidth() / 2),
				yMundo + getHeight() + distVertical, distVertical);
		if (yTrans == 0)
			finalizaSalto();
		else {
			trasladar(0, yTrans);
			yMundo += yTrans;
		}
	}

	private void finalizaSalto() {
		movVertical = SIN_SALTO;
		contSubiendo = 0;
		if (quieto) {
			if (derecha)
				setImage("derecha");
			else
				setImage("izquierda");
		}
	}
}