package com.safari.rhino;

import java.awt.image.BufferedImage;

public class ImagesPlayer {

	private String imgNombre;
	private int animPeriod;
	private double seqDuration;
	private boolean seRepite, ticksIgnorados;
	private ImaCharger imsLoader;
	private long tiempoTotalAnim;
	private int numImagenes;
	private int imPosition;
	private int mostrarPeriodo;
	private ObservadorImagesPlayer observador;

	public ImagesPlayer(String nm, int ap, double d, boolean isr, ImaCharger il) {

		imgNombre = nm;
		animPeriod = ap;
		seqDuration = d;
		seRepite = isr;
		imsLoader = il;
		tiempoTotalAnim = 0L;

		if (seqDuration < 0.5) {
			System.out.println("Duracion minima de secuencia es 0.5");
			seqDuration = 0.5;
		}

		if (!imsLoader.estaCargado(imgNombre)) {
			System.out.println("El archivo " + imgNombre
					+ " No es conocido por ImaCharger");
			numImagenes = 0;
			imPosition = -1;
			ticksIgnorados = true;
		}

		else {
			numImagenes = imsLoader.numImagenes(imgNombre);
			imPosition = 0;
			ticksIgnorados = false;
			mostrarPeriodo = (int) (1000 * seqDuration / numImagenes);

		}
	}// Fin del constructor Parametrico

	public void actualizarTick() {
		if (!ticksIgnorados) {
			tiempoTotalAnim = (tiempoTotalAnim + animPeriod)
					% (long) (1000 * seqDuration);
			imPosition = (int) (tiempoTotalAnim / mostrarPeriodo);
			if ((imPosition == numImagenes - 1) && (!seRepite)) {
				ticksIgnorados = true;
				if (observador != null)
					observador.secuenciaFin(imgNombre); // call callback
			}

		}
	}

	public void stop() {
		ticksIgnorados = true;
	}

	public BufferedImage obtenerCurrentImage() {
		if (numImagenes != 0)
			return imsLoader.getImagesBuff(imgNombre, imPosition);
		else
			return null;
	} // fin de obtenerCurrentImage()

}
