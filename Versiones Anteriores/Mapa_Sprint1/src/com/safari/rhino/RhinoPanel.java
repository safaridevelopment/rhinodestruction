package com.safari.rhino;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

public class RhinoPanel extends JPanel implements KeyListener {

	public static int panel_w = 500;
	public static int panel_h = 235;
	public static String image_info = "infoImagenes.txt";
	public static String ladrillos_info = "infoLadrillos.txt";
	public ManejadorDeLadrillos ladrillin;
	public Font msjFont;
	public FontMetrics metrics;
	public long gameStartTime;
	public volatile boolean running = false;

	public Graphics dbg;
	public Image dbImage = null;
	public RhinoGoblin gob;
	// ///////////
	public ManejadorDeColision col;

	// ///////////

	public RhinoPanel() {
		addKeyListener(this);
		setDoubleBuffered(false);

		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(panel_w, panel_h));
		setFocusable(true);

		requestFocus();
		ImaCharger imgLoad = new ImaCharger(image_info);
		col = new ManejadorDeColision(panel_w, panel_h, ladrillos_info, imgLoad);
		ladrillin = new ManejadorDeLadrillos(panel_w, panel_h, ladrillos_info,
				imgLoad);

		int velocidadLadrillo = ladrillin.getVelocidad();

		msjFont = new Font("SansSerif", Font.BOLD, 24);
		gob = new RhinoGoblin(0, 0, velocidadLadrillo, ladrillin, imgLoad, 1);
		metrics = this.getFontMetrics(msjFont);

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				running = true;

				while (running) {
					updateJuego();
					juegoRender();
					paintScreen();

					try {

						Thread.sleep(3);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		};
		Thread thread = new Thread(runnable);
		thread.start();

	}

	private void paintScreen() {
		Graphics g;
		try {
			g = this.getGraphics();
			if ((g != null) && (dbImage != null))
				g.drawImage(dbImage, 0, 0, null);
			Toolkit.getDefaultToolkit().sync();
			g.dispose();
		} catch (Exception e) {
			System.out.println("Error de grafico " + e);
		}
	} // end of paintScreen()

	private void updateJuego() {
		ladrillin.update();
		gob.updateGoblin();
	}

	private void juegoRender() {

		if (dbImage == null) {
			dbImage = createImage(panel_w, panel_h);
			if (dbImage == null) {
				System.out.println("dbImage no tiene nada");
				return;
			} else
				dbg = dbImage.getGraphics();
		}

		dbg.setColor(Color.GRAY);
		dbg.fillRect(0, 0, panel_w, panel_h);

		ladrillin.display(dbg);
		gob.dibujarRhinoGoblin(dbg);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			if (gob.chequearParedeIzquierda()) {
				ladrillin.movQuieto();

			} else {
				gob.movIzquierda(true);
				ladrillin.movDerecha();
			}

			// gob.mover(false, true);
			ladrillin.movIzquierda();
		}
		if (key == KeyEvent.VK_RIGHT) {
			if (gob.chequearParedeDerecha()) {
				ladrillin.movQuieto();

			} else {
				gob.movDerecha(true);
				ladrillin.movDerecha();
			}

			// gob.mover(true, false);

		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			gob.movIzquierda(false);
			ladrillin.movQuieto();
		}

		if (key == KeyEvent.VK_RIGHT) {

			gob.movDerecha(false);
			ladrillin.movQuieto();
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
