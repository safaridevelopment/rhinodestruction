package com.safari.rhino;

import java.awt.Container;

import javax.swing.JFrame;

public class RhinoDestruction extends JFrame {
	
	private RhinoPanel jp;
	
	public RhinoDestruction() {
		Container c = getContentPane();
		jp = new RhinoPanel();
		c.add(jp);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 360);
		setVisible(true);
		setResizable(false);
	}


	public static void main(String[] args) {
		new RhinoDestruction();
	}
}
