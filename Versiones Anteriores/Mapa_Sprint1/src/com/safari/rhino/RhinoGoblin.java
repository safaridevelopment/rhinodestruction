package com.safari.rhino;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class RhinoGoblin extends Goblin {

	private double DURACION = 0.5;

	private static int no_salta = 0;
	private static int subiendo = 1;
	private static int gravedad = 2;
	private static int velocidad;
	private int periodo;
	private ManejadorDeLadrillos ladrillin;

	private boolean derecha;

	private int xMundo;

	private int yMundo;

	private boolean quieto;

	private boolean izquierda;
	// ///////working here////////

	public int posColision;

	public ManejadorDeColision colision;
	public static String ladrillos_info = "infoLadrillos.txt";
	public int posX;
	public int posY;

	// //////////////////////////
	public RhinoGoblin(int w, int h, int velocidadLadrillo,
			ManejadorDeLadrillos ml, ImaCharger ic, int p) {

		super(w / 2, h / 2, w, h, ic, "rhinorun");
		// ///////////////////////
		colision = new ManejadorDeColision(w, h, ladrillos_info, ic);

		posX = xlock;
		posY = ylock;

		// ////////////////////////
		periodo = p;
		velocidad = velocidadLadrillo;
		ladrillin = ml;
		setPaso(0, 0);

		derecha = true;
		quieto = true;

		// ylock = ladrillin.encontrarPiso(xlock + getWidth()/2 )-getHeight();
		ylock = posY;
		xMundo = posX;
		yMundo = ylock;

	}

	public void chequearGravedad() {
		if (colision.checkColision(getposX(), getposY())) {
			// nada
		} else {
			posY = posY + gravedad;
		}

	}

	public boolean chequearParedeDerecha() {
		if (colision.checkColision(getposX() + getWidth(), getposY()
				- getHeight())) {
			posColision = getposX();
			sinMovimiento();
			return true;
		} else {
			return false;// movDerecha(true);
		}

	}

	public boolean chequearParedeIzquierda() {
		if (colision.checkColision(getposX(), getposY() - getHeight())) {
			posColision = getposX();
			sinMovimiento();
			return true;
		} else {
			return false;// movDerecha(true);
		}

	}

	public void movDerecha(boolean t) {
		setImagen("rhinorun");
		if (t) {
			posX = posX + 10;
			derecha = true;
			izquierda = false;
			quieto = false;
		} else {
			derecha = false;
		}

	}

	public void movIzquierda(boolean t) {
		setImagen("rhinoleft");
		if (t) {
			posX = posX - 10;
		}
		izquierda = true;
		derecha = false;
		quieto = false;

	}

	public void sinMovimiento() {
		izquierda = false;
		derecha = false;
		quieto = true;
	}

	public void dibujarRhinoGoblin(Graphics g) {

		if (estaActivo()) {
			if (imagen == null) {
				g.setColor(Color.RED);
				g.fillOval(xlock, ylock, SIZE, SIZE);
				g.setColor(Color.black);

			} else {
				if (isLooping) {
					imagen = player.obtenerCurrentImage();

				}
				chequearGravedad();

				g.drawImage(imagen, getposX(), getposY(), null);

			}
			// Quedaste aca para imprimir el goblin!
		}
	}

	private int getposX() {
		return posX;
	}

	private int getposY() {
		return posY;
	}

}
