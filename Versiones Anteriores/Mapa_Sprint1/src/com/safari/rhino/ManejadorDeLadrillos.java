package com.safari.rhino;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ManejadorDeLadrillos {

	public static String img_dir = "Images/";
	public static double MOVE_FACTOR = 0.005;
	public static int MAX_LINEA_LADRILLOS = 15;
	public int panel_w;
	public int panel_h;
	public int ladrillo_w, ladrillo_h;
	public int xCabezaMapa;
	public boolean mueveDerecha; // movement flags
	public boolean mueveIzquierda;
	public int numColumn, numFilas;

	public ArrayList listaLadrillos;

	public ImaCharger imaCharger;
	public ArrayList ImgLadrillos = null;

	public ArrayList[] columnLadrillos;
	public int width, height;
	public int velocidad;

	public ManejadorDeLadrillos(int ancho, int alto, String plano, ImaCharger il) {

		panel_h = alto;
		panel_w = ancho;
		imaCharger = il;

		listaLadrillos = new ArrayList();
		loadLadrillo(plano);
		initInfoLadrillos();
		crearColumnas();

		velocidad = (int) (ladrillo_w * MOVE_FACTOR);
		if (velocidad == 0) {

			velocidad = 1;
		}
		xCabezaMapa = 0;
		mueveIzquierda = false;
		mueveDerecha = false;
	}

	private void initInfoLadrillos() {
		if (ImgLadrillos == null) {

			System.exit(1);
		}

		if (listaLadrillos.size() == 0) {

			System.exit(1);
		}

		BufferedImage img = (BufferedImage) ImgLadrillos.get(0);
		ladrillo_w = img.getWidth();
		ladrillo_h = img.getHeight();

		findNumLadrillos();
		calcDimensionesMapa();
		chequearVacios();
		detallesLadrillo();

	}

	private void detallesLadrillo() {
		Ladrillo b;
		BufferedImage im;
		for (int i = 0; i < listaLadrillos.size(); i++) {
			b = (Ladrillo) listaLadrillos.get(i);
			im = (BufferedImage) ImgLadrillos.get(b.getimagenID());
			b.establecerImagen(im);
			b.setcoordY(panel_h, numFilas);
		}

	}

	private void findNumLadrillos() {
		Ladrillo b;
		numColumn = 0;
		numFilas = 0;
		for (int i = 0; i < listaLadrillos.size(); i++) {
			b = (Ladrillo) listaLadrillos.get(i);
			if (numColumn < b.getmapX())
				numColumn = b.getmapX();
			if (numFilas < b.getmapY())
				numFilas = b.getmapY();
		}
		numColumn++; // since we want the max no., not the max coord
		numFilas++;

	}

	private void loadLadrillo(String plano) {

		String directorio = img_dir + plano;
		System.out.println("Cargando Imagen: " + directorio);

		int numTiraImagenes = -1;
		int numLineaLadrillos = 0;

		try {
			BufferedReader br = new BufferedReader(new FileReader(directorio));
			String linea;
			char ch;

			while ((linea = br.readLine()) != null) {
				if (linea.length() == 0)
					continue;
				if (linea.startsWith("//"))
					continue;
				ch = Character.toLowerCase(linea.charAt(0));
				if (ch == 's') {
					numTiraImagenes = getTiraImagenes(linea);
				} else {
					if (numLineaLadrillos > MAX_LINEA_LADRILLOS)
						System.out
								.println("Superaste la cantidad de lineas de ladrillos maximas");

					else if (numTiraImagenes == -1)
						System.out
								.println("No has Cargado ninguna tira de imagenes");
					else {
						guardarLadrillos(linea, numLineaLadrillos,
								numTiraImagenes);
						numLineaLadrillos++;
					}
				}
			}

			br.close();
		} catch (IOException e) {
			System.out.println("Error Leyendo Archivo: " + directorio);
			System.exit(1);
		}
	}

	private void guardarLadrillos(String linea, int lineaNum, int numImagenes) {
		int imagenID;
		char ch;
		for (int x = 0; x < linea.length(); x++) {
			ch = linea.charAt(x);
			if (ch == ' ')
				continue;
			if (Character.isDigit(ch)) {
				imagenID = ch - '0';
				if (imagenID >= numImagenes)
					System.out.println("La ID de la imagen: " + imagenID
							+ " No se encuentra");
				else
					listaLadrillos.add(new Ladrillo(imagenID, x, lineaNum));
			} else
				System.out.println(ch + " No es un numero");
		}
	}

	public int getVelocidad() {
		return velocidad;
	}

	private int getTiraImagenes(String linea) {
		StringTokenizer token = new StringTokenizer(linea);
		if (token.countTokens() != 3) {
			System.out.println("cantidad invalida para " + linea);
			return -1;
		} else {
			token.nextToken();
			System.out.println("Bricks strip: ");

			String directorio = token.nextToken();
			int numero = -1;

			try {
				numero = Integer.parseInt(token.nextToken());
				imaCharger.CargarTiraDeImagenes(directorio, numero);
				ImgLadrillos = imaCharger.getImages(getPrefix(directorio));
			} catch (Exception e) {
				System.out.println("Numero incorrecto pasa " + linea);
			}
			return numero;
		}
	}// Fin de get tira imagenes

	private String getPrefix(String directorio) {
		int posn;
		if ((posn = directorio.lastIndexOf(".")) == -1) {
			System.out.println("Ningun prefijo encontrado");
			return directorio;
		} else {
			return directorio.substring(0, posn);
		}
	}// Fin get Prefix

	private void crearColumnas() {
		columnLadrillos = new ArrayList[numColumn];
		for (int i = 0; i < numColumn; i++) {
			columnLadrillos[i] = new ArrayList();
		}
		Ladrillo b;
		for (int j = 0; j < listaLadrillos.size(); j++) {
			b = (Ladrillo) listaLadrillos.get(j);
			columnLadrillos[b.getmapX()].add(b);
		}
	}// Fin de crear Coloumnas

	private void chequearVacios() {
		boolean[] hasLadrillo = new boolean[numColumn];
		for (int j = 0; j < numColumn; j++) {
			hasLadrillo[j] = false;
		}

		Ladrillo b;
		for (int i = 0; i < listaLadrillos.size(); i++) {
			b = (Ladrillo) listaLadrillos.get(i);
			if (b.getmapY() == numFilas - 1)
				hasLadrillo[b.getmapX()] = true;
		}

		for (int j = 0; j < numColumn; j++)
			if (!hasLadrillo[j]) {
				System.out.println("Vacio encontrado en el mapa en " + j);
				System.exit(0);
			}
	}// Fin de chequear Vacios

	private void calcDimensionesMapa() {
		width = ladrillo_w * numColumn;
		height = ladrillo_h * numFilas;

		if (panel_w < panel_w) {
			System.out
					.println("El mapa de ladrillos es mas pequeno que el panel");
			System.exit(0);
		}
	}// Fin calc Dimensiones Mapa

	public void display(Graphics g) {
		int bCoord = (int) (xCabezaMapa / ladrillo_w) * ladrillo_w;
		int offset;

		if (bCoord >= 0)
			offset = xCabezaMapa - bCoord;
		else
			offset = bCoord - xCabezaMapa;

		if ((bCoord >= 0) && (bCoord < panel_w)) {
			dibujarLadrillos(g, 0 - (ladrillo_w - offset), xCabezaMapa, width
					- bCoord - ladrillo_w);
			dibujarLadrillos(g, xCabezaMapa, panel_w, 0);
		}

		else if (bCoord >= panel_w)
			dibujarLadrillos(g, 0 - (ladrillo_w - offset), panel_w, width
					- bCoord - ladrillo_w);
		else if ((bCoord < 0) && (bCoord >= panel_w - width + ladrillo_w))
			dibujarLadrillos(g, 0 - offset, panel_w, -bCoord);
		else if (bCoord < panel_w - width + ladrillo_w) {
			dibujarLadrillos(g, 0 - offset, width + xCabezaMapa, -bCoord); // bm
																			// tail
			dibujarLadrillos(g, width + xCabezaMapa, panel_w, 0); // bm start
		}
	}

	private void dibujarLadrillos(Graphics g, int xStart, int xEnd,
			int xLadrillo) {

		int xMapa = xLadrillo / ladrillo_w;
		ArrayList columna;
		Ladrillo b;
		for (int x = xStart; x < xEnd; x += ladrillo_w) {
			columna = columnLadrillos[xMapa];
			for (int i = 0; i < columna.size(); i++) {
				b = (Ladrillo) columna.get(i);
				b.display(g, x);
			}
			xMapa++;
		}
	}// Fin dibujarLadrillos

	public void movDerecha() {
		mueveDerecha = true;
		mueveIzquierda = false;
	}

	public void movIzquierda() {
		mueveDerecha = false;
		mueveIzquierda = true;
	}

	public void movQuieto() {
		mueveDerecha = false;
		mueveIzquierda = false;
	}

	public void update() {
		if (mueveDerecha) {
			xCabezaMapa = (xCabezaMapa - velocidad) % width;
		}

		else if (mueveIzquierda) {
			xCabezaMapa = (xCabezaMapa + velocidad) % width;
		}
	}

	public int encontrarPiso(int xGoblin) {
		int xMap = (int) (xGoblin / ladrillo_w);

		int locY = panel_h;
		ArrayList column = columnLadrillos[xMap];

		Ladrillo l;
		for (int i = 0; i < column.size(); i++) {
			l = (Ladrillo) column.get(i);
			if (l.getcoordY() < locY)
				locY = l.getcoordY();
		}
		return locY;
	} // fin de encopntrar piso
}
