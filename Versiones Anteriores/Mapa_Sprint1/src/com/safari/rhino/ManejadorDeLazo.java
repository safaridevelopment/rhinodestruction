package com.safari.rhino;

import java.awt.*;

import org.omg.CORBA.PUBLIC_MEMBER;

public class ManejadorDeLazo{

	private String ribImages[] = { "edificios" };
	private double constanteMovimiento[] = { 1 };// si es 0 no se
															// mueve

	private Lazo[] lazos;
	private int numLazos;
	private int tamanoMov;// la distancia en la q se mueve el lazo
	
	
	

	public ManejadorDeLazo(int w, int h, int ladrilloTamMov, ImaCharger imsLd) {
	
		tamanoMov=ladrilloTamMov;
		numLazos=ribImages.length;
		lazos=new Lazo[numLazos];
		
		for (int i = 0; i < numLazos; i++) {
			lazos[i] = new Lazo (w, h, imsLd.cargarImg(ribImages[i]), (int) (constanteMovimiento[i]*tamanoMov));
		}
		
	}

	public void moverDer() {
		for (int j = 0; j < numLazos; j++) {
			lazos[j].moverDer();
		}
	}

	public void moverIzq() {
		for (int j = 0; j < numLazos; j++) {
			lazos[j].moverIzq();
		}
	}

	public void quieto() {
		for (int j = 0; j < numLazos; j++) {
			lazos[j].quieto();
		}
	}

	public void update() {
		for (int j = 0; j < numLazos; j++) {
			lazos[j].update();
		}

	}

	public void display(Graphics g) {
		for (int j = 0; j < numLazos; j++) {
			lazos[j].display(g);
		}
	}

}
