package com.safari.rhino;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JPanel;

import com.sun.j3d.utils.timer.J3DTimer;

public class RhinoPanel extends JPanel implements Runnable, KeyListener {
	private int NO_DELAYS_PER_YIELD = 16; // Numero de frames con un retraso de
											// 0 ms antes de que el thread se
											// cague
	private int MAX_FRAME_SKIPS = 5; // Numero de frames que se pueden obviar en
										// 1 loop de animacion ;

	private Thread animator;
	public static int panel_w = 650;
	public static int panel_h = 235;
	public static String image_info = "infoImagenes.txt";
	public static String ladrillos_info = "infoLadrillos.txt";
	public ManejadorDeLadrillos ladrillin;
	public Font msjFont;
	public FontMetrics metrics;
	public long gameStartTime;
	public volatile boolean running = false;
	private RhinoDestruction rhino;
	public Graphics dbg;
	public Image dbImage = null;
	public RhinoGoblin gob;
	public ManejadorDeLazo lazo;
	// ///////////
	public ManejadorDeColision col;
	private long periodo;

	// ///////////

	public RhinoPanel(RhinoDestruction rd, long p) {
		rhino = rd;
		this.periodo = p;
		addKeyListener(this);
		setDoubleBuffered(false);

		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(panel_w, panel_h));
		setFocusable(true);

		requestFocus();
		ImaCharger imgLoad = new ImaCharger(image_info);
		col = new ManejadorDeColision(panel_w, panel_h, ladrillos_info, imgLoad);
		ladrillin = new ManejadorDeLadrillos(panel_w, panel_h, ladrillos_info,
				imgLoad);

		int velocidadLadrillo = ladrillin.getVelocidad();

		lazo = new ManejadorDeLazo(panel_w, panel_h, velocidadLadrillo, imgLoad);

		msjFont = new Font("SansSerif", Font.BOLD, 24);
		gob = new RhinoGoblin(panel_w, panel_h, velocidadLadrillo, ladrillin,
				imgLoad, (int) (periodo / 1000000L));
		metrics = this.getFontMetrics(msjFont);
	}

	private void startGame()
	{
		if (animator == null || !running) {
			animator = new Thread(this);
			animator.start();
		}
	} 

	public void run() {
		long beforeTime, afterTime, timeDiff, sleepTime;
		long overSleepTime = 0L;
		int noDelays = 0;
		long excess = 0L;

		gameStartTime = J3DTimer.getValue();
		beforeTime = gameStartTime;

		running = true;

		while (running) {

			updateJuego();
			juegoRender();
			paintScreen();

			afterTime = J3DTimer.getValue();
			timeDiff = afterTime - beforeTime;
			sleepTime = (periodo - timeDiff) - overSleepTime;

			if (sleepTime > 0) {
				try {
					Thread.sleep(sleepTime / 100000000L); // nano -> ms
				} catch (InterruptedException ex) {
				}
				overSleepTime = (J3DTimer.getValue() - afterTime) - sleepTime;
			} else {
				excess -= sleepTime;
				overSleepTime = 0L;

				if (++noDelays >= NO_DELAYS_PER_YIELD) {
					Thread.yield();
					noDelays = 0;
				}
			}

			beforeTime = J3DTimer.getValue();
			int skips = 0;
			while ((excess > periodo) && (skips < MAX_FRAME_SKIPS)) {
				excess -= periodo;
				updateJuego();
				skips++;
			}
		}
		System.exit(0);
	}
	
	  public void addNotify()
	  { super.addNotify();   
	    startGame();         
	  }


	private void paintScreen() {
		Graphics g;
		try {
			g = this.getGraphics();
			if ((g != null) && (dbImage != null))
				g.drawImage(dbImage, 0, 0, null);
			Toolkit.getDefaultToolkit().sync();
			g.dispose();
		} catch (Exception e) {
			System.out.println("Error de grafico " + e);
		}
	} // end of paintScreen()

	private void updateJuego() {

		if (gob.colisionaLadrillo()) {
			gob.sinMovimiento();
			ladrillin.movQuieto();
			System.out.println("COLISION");
		}
		
		ladrillin.update();
		gob.updateGoblin();
		
	}

	private void juegoRender() {

		if (dbImage == null) {
			dbImage = createImage(panel_w, panel_h);
			if (dbImage == null) {
				System.out.println("dbImage no tiene nada");
				return;
			} else
				dbg = dbImage.getGraphics();
		}

		dbg.setColor(Color.GRAY);
		dbg.fillRect(0, 0, panel_w, panel_h);

		lazo.display(dbg);
		ladrillin.display(dbg);
		gob.dibujarGoblin(dbg);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {

			gob.movIzquierda();
			lazo.moverDer();

			ladrillin.movIzquierda();
		}
		if (key == KeyEvent.VK_RIGHT) {

			gob.movDerecha();
			ladrillin.movDerecha();
			lazo.moverIzq();
		}
		// if (key == KeyEvent.VK_UP) {
		// gob.salto();
		// }

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			gob.sinMovimiento();
			ladrillin.movQuieto();
			lazo.quieto();
		}

		if (key == KeyEvent.VK_RIGHT) {

			gob.sinMovimiento();
			ladrillin.movQuieto();
			lazo.quieto();
		}
		// if (key == KeyEvent.VK_UP) {
		// gob.finalizaSalto();
		// }
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
