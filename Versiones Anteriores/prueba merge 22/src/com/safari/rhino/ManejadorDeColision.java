package com.safari.rhino;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class ManejadorDeColision   {
	public ManejadorDeLadrillos ladrillin;
	public boolean colision;
	

	public ManejadorDeColision(int ancho, int alto, String plano, ImaCharger il ) {
		ladrillin = new ManejadorDeLadrillos(ancho, alto, plano, il);
		colision = false;
	}

	public boolean checkCaida(int x, int y) {
		if (checkColision(x, y)) {
			return false;
		} else {
			// chek colision dos bloques despues
		}

		return false;
	}
	
	public boolean checkColision(int x, int y) {
		int columPixel, filaPixel, arrayColum, arrayFila;

		columPixel = transPixelAColumna();
		filaPixel = transPixelAFila();

		arrayColum = (x / columPixel);
		arrayFila = (y / filaPixel);
		
		if (getTipoLadrillo(arrayColum, arrayFila) > 500) {
			return false;
		} else {
			return true;
		}
	}

	public int transPixelAColumna() {
		int r;
		r = ladrillin.ladrillo_w;
		return r;
	}

	public int transPixelAFila() {
		int r;
		r = ladrillin.ladrilloH;
		return r;
	}

	public int getTipoLadrillo(int colum, int filainvrsa) {
		Ladrillo t;
		ArrayList r = ladrillin.columnLadrillos[colum];
		int p;
		if (filainvrsa <= ladrillin.numFilas - r.size()) {
			return 846546;
		} else {
			t = (Ladrillo) r.get(((-1) * (ladrillin.numFilas - filainvrsa + 1 - r.size())));
			return t.getimagenID();

		}
	}
}
// addMouseListener(new MouseListener() {
//
// @Override
// public void mouseReleased(MouseEvent arg0) {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public void mousePressed(MouseEvent arg0) {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public void mouseExited(MouseEvent arg0) {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public void mouseEntered(MouseEvent arg0) {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public void mouseClicked(MouseEvent arg0) {
//
// int posXPlano, columPixel, posYPlano, filaPixel, arrayColum, arrayFila;
// posXPlano = arg0.getX();
// columPixel = transPixelAColumna();
// posYPlano = arg0.getY();
// filaPixel = transPixelAFila();
//
// System.out.println("Columna " + (posXPlano / columPixel));
// System.out.println("Fila " + (posYPlano / filaPixel));
// arrayColum = (posXPlano / columPixel);
// arrayFila = (posYPlano / filaPixel);
// System.out.println("El ladrillo es "
// + getTipoLadrillo(arrayColum, arrayFila));
// }
// });