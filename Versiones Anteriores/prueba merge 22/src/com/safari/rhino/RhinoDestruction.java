package com.safari.rhino;

import static java.lang.ClassLoader.*;

import java.awt.Container;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class RhinoDestruction extends JFrame {
	
	private static int FPS=30;
	Image icono;
	ImageIcon i = new ImageIcon("Images/rhinodestruction.jpg");
	

	private RhinoPanel jp;
	
	public RhinoDestruction(long periodo) {
		super("Rhino Destruction");
		icono = i.getImage();
		Container c = getContentPane();
		jp = new RhinoPanel(this, periodo);
		c.add(jp, "Center");
		setTitle("Rhino Destruction");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 265);
		setVisible(true);
		setResizable(false);
		setIconImage(icono);
	}


	public static void main(String[] args) {
		long period= (long)  1000/FPS;
		new RhinoDestruction(period*1000000L); //ms a nanosecs
	}
}
