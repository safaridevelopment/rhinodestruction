package com.safari.rhino;

import java.awt.Graphics;
import java.awt.image.*;

public class Ladrillo {
	
	private BufferedImage imagen; 
	private int mapX, mapY; 
	public int imagenID;
	private int altura;
	private int coordY; 
	
	public Ladrillo(int ID, int x, int y) {
		
		imagenID=ID;
		mapX=x;
		mapY=y;
		
	}
	
	public int getmapX() {
		return mapX;
	}
	
	public int getmapY() {
		return mapY;
	}
	
	public int getimagenID() {
		return imagenID;
	}
	
	public void establecerImagen(BufferedImage img) {
		imagen=img;
		altura=img.getHeight();				
	}
	
	public void setcoordY(int paltura, int maxLadrillosY) {
		coordY = paltura - ((maxLadrillosY-mapY) * altura);
	}
	
	 public int getcoordY() {
	    return coordY;  
	  }
	 
	 public void display(Graphics g2d, int xScr) {
		 g2d.drawImage(imagen, xScr, coordY, null);
	 }
}


