package com.safari.rhino;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Goblin {

	public static int XPASO = 5;
	public static int YPASO = 5;
	public static int SIZE = 12;

	public ImaCharger imsLoader;
	public String imgNombre;
	public BufferedImage imagen;
	public int width, height;

	public ImagesPlayer player;
	public boolean isLooping;
	public boolean estaActivo = true;

	public int xlock;
	public int ylock;
	public int dx = 2;
	public int dy;
	public int panel_w;
	public int panel_h;

	//
	public Goblin() {
		// TODO Auto-generated constructor stub
	}

	public Goblin(int x, int y, int w, int h, ImaCharger imsLd, String nombre) {
		xlock = x;
		ylock = y;
		panel_w = w;
		panel_h = h;
		dx = XPASO;
		dy = YPASO;
		imsLoader = imsLd;
		setImagen(nombre);
	}

	public void setImagen(String nombre) {
		imgNombre = nombre;
		imagen = imsLoader.getImagesBuff(imgNombre);
		if (imagen == null) {
			System.out.println("No hay una imagen goblin para " + imgNombre);
			width = SIZE;
			height = SIZE;
		} else {
			width = imagen.getWidth();
			height = imagen.getHeight();
		}

		player = null;
		isLooping = false;
	}

	public void loopImagen(int animPeriod, double seqDuration) {
		if (imsLoader.numImagenes(imgNombre) >= 1) {
			player = null;
			player = new ImagesPlayer(imgNombre, animPeriod, seqDuration, true,
					imsLoader);
			isLooping = true;

		} else {
			System.out.println(imgNombre + " No es una secuencia de imagenes");
		}

	}// Fin de loopImagen

	public void stopLooping() {
		if (isLooping) {
			player.stop();
			isLooping = false;
		}
	} // fin de stopLooping()

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getPanel_w() {
		return panel_w;
	}

	public int getPanel_h() {
		return panel_h;
	}

	public void setPosition(int x, int y) {
		xlock = x;
		ylock = y;
	}

	public int getPasoX() {
		return dx;
	}

	public int getPasoY() {
		return dy;
	}

	public void setPaso(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	public boolean estaActivo() {
		return estaActivo;
	}

	public void setActivo(boolean a) {
		estaActivo = a;
	}

	public int getPosX() {
		return xlock;
	}

	public int getPosY() {
		return ylock;
	}

	public Rectangle getMyRectangle() {
		return new Rectangle(xlock, ylock, width, height);
	}

	public void updateGoblin() {
		if (estaActivo()) {
			xlock += dx;
			ylock += dy;
			if (isLooping)
				player.actualizarTick();
		}
	} // Fin de updateGoblin()

	public void dibujarGoblin(Graphics g) {

		if (estaActivo()) {
			if (imagen == null) {
				g.setColor(Color.RED);
				g.fillOval(xlock, ylock, SIZE, SIZE);
				g.setColor(Color.black);

			} else {
				if (isLooping)
					imagen = player.obtenerCurrentImage();
				g.drawImage(imagen, xlock, ylock, null);
			}

			// Quedaste aca para imprimir el goblin!
		}
	}

	public void trasladar(int xDist, int yDist) {
		xlock += xDist;
		ylock += yDist;
	}

}
