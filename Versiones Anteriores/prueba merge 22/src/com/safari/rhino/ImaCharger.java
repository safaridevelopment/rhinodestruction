package com.safari.rhino;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

public class ImaCharger {
	private static final String IMAGE_DIR = "Images/";
	private GraphicsConfiguration gc;
	private HashMap imagesMap;
	private HashMap gNombresMap;
	
	public ImaCharger() {
		InitCargador();
	}

	private void InitCargador() {
		imagesMap = new HashMap();
		gNombresMap = new HashMap();

		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		gc = ge.getDefaultScreenDevice().getDefaultConfiguration();

	}

	// /////////////////////////
	public ImaCharger(String plano) {
		InitCargador();
		cargadorImgFile(plano);
	}

	private void cargadorImgFile(String plano) {
		String directorio = IMAGE_DIR + plano;
		System.out.println("Leyendo archivo: " + directorio);
		try {
			InputStream in = this.getClass().getResourceAsStream(directorio);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			String linea;
			char ch;
			while ((linea = br.readLine()) != null) {
				if (linea.length() == 0) {
					continue;
				}
				if (linea.startsWith("//")) {
					continue;
				}
				ch = Character.toLowerCase(linea.charAt(0));
				if (ch == 'o') {
					getNombreArchivoImg(linea);
				} else {
					if (ch == 'n') {
						getNumeracionImg(linea);
					} else {
						if (ch == 's') {
							getTiraImg(linea);
						} else {
							if (ch == 'g') {
								getGrupoImg(linea);
							} else {
								System.out.println("No se reconoce la linea "
										+ linea);
							}

						}
					}
				}
			}

			br.close();
		} catch (IOException e) {
			System.out.println("Error leyendo el archivo: " + directorio);
			System.exit(1);
		}
	} // Fin de cargador Img File

	private void getGrupoImg(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() < 3) {
			System.out
					.println("Error en el numero de argumentos para " + linea);
		} else {
			tokens.nextToken();

			System.out.print("g Linea: ");

			String nombre = tokens.nextToken();

			ArrayList listadirecta = new ArrayList();
			listadirecta.add(tokens.nextToken());
			while (tokens.hasMoreTokens())
				listadirecta.add(tokens.nextToken());

			cargarGrupoImg(nombre, listadirecta);
		}

	}// Fin de get Grupo de Img

	private int cargarGrupoImg(String nombre, ArrayList listadirecta) {
		if (imagesMap.containsKey(nombre)) {
			System.out.println("Error: " + nombre + "already used");
			return 0;
		}

		if (listadirecta.size() == 0) {
			System.out.println("List of filenames is empty");
			return 0;
		}

		BufferedImage bi;
		ArrayList nms = new ArrayList();
		ArrayList imsList = new ArrayList();
		String nm, fnm;
		int loadCount = 0;

		System.out.println("  Adding to " + nombre + "...");
		System.out.print("  ");
		for (int i = 0; i < listadirecta.size(); i++) { // load the files
			fnm = (String) listadirecta.get(i);
			nm = getPrefix(fnm);
			// getPrefix(fnm);

			if ((bi = CargadorImage(fnm)) != null) {
				loadCount++;
				imsList.add(bi);
				nms.add(nm);
				System.out.print(nm + "/" + fnm + " ");
			}
		}
		System.out.println();

		if (loadCount == 0)
			System.out.println("No images loaded for " + nombre);
		else {
			imagesMap.put(nombre, imsList);
			gNombresMap.put(nombre, nms);
		}

		return loadCount;
	}

	private void getTiraImg(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() != 3) {
			System.out.println("Numero invalido de lineaas " + linea);
		} else {
			tokens.nextToken();
		}
		System.out.print("s Linea: ");

		String directorio = tokens.nextToken();
		int numero = -1;
		try {
			numero = Integer.parseInt(tokens.nextToken());
		} catch (Exception e) {
			System.out.println("Numero incorrecto para " + linea);
		}

		CargarTiraDeImagenes(directorio, numero);

	}// Fin de get tira IMg

	private void getNumeracionImg(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() != 3)
			System.out.println("Error en numero de argumentos " + linea);
		else {
			tokens.nextToken();
			System.out.print("n Linea: ");

			String fnm = tokens.nextToken();
			int number = -1;
			try {
				number = Integer.parseInt(tokens.nextToken());
			} catch (Exception e) {
				System.out.println("Numero es incorrecto " + linea);
			}
			cargarNumeroImg(fnm, number);
		}
	}// Fin de get Numeracion Imag

	private int cargarNumeroImg(String directorio, int numero) {
		String prefix = null;
		String postfix = null;
		int starPosn = directorio.lastIndexOf("*"); // find the '*'
		if (starPosn == -1) {
			System.out.println("No '*' in filename: " + directorio);
			prefix = getPrefix(directorio);
		} else { // treat the fnm as prefix + "*" + postfix
			prefix = directorio.substring(0, starPosn);
			postfix = directorio.substring(starPosn + 1);
		}
		if (imagesMap.containsKey(prefix)) {
		      System.out.println( "Error: " + prefix + "already used");
		      return 0;
		    }

		    return cargarNumeroImg(prefix, postfix, numero);

	}

	private int cargarNumeroImg(String prefix, String postfix, int numero) {
		String imFnm;
	    BufferedImage bi;
	    ArrayList imsList = new ArrayList();
	    int loadCount = 0;

	    if (numero <= 0) {
	      System.out.println("Error: Number <= 0: " + numero);
	      imFnm = prefix + postfix;
	      if ((bi = cargarImg(imFnm)) != null) {
	        loadCount++;
	        imsList.add(bi);
	        System.out.println("  Stored " + prefix + "/" + imFnm);
	      }
	    }
	    else {   // load prefix + <i> + postfix, where i = 0 to <number-1>
	      System.out.print("  Adding " + prefix + "/" + 
	                           prefix + "*" + postfix + "... ");
	      for(int i=0; i < numero; i++) {
	        imFnm = prefix + i + postfix;
	        if ((bi = cargarImg(imFnm)) != null) {
	          loadCount++;
	          imsList.add(bi);
	          System.out.print(i + " ");
	        }
	      }
	      System.out.println();
	    }

	    if (loadCount == 0)
	      System.out.println("No images loaded for " + prefix);
	    else 
	      imagesMap.put(prefix, imsList);

	    return loadCount;
	}

	private BufferedImage cargarImg(String fnm) {
		try {
		       BufferedImage im =  ImageIO.read( 
		                      getClass().getResource(IMAGE_DIR + fnm) );
		       	       
		       int transparency = im.getColorModel().getTransparency();
		       BufferedImage copy =  gc.createCompatibleImage(
		                                im.getWidth(), im.getHeight(),
				                        transparency );
		       
		       Graphics2D g2d = copy.createGraphics();
		       
		       g2d.drawImage(im,0,0,null);
		       g2d.dispose();
		       return copy; 
		 }catch(IOException e) {
	         System.out.println("Load Image error for " +
	                       IMAGE_DIR + "/" + fnm + ":\n" + e); 
	         return null;
	       }
	}

	private void getNombreArchivoImg(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() != 2)
			System.out.println("Error en numbero de lineas " + linea);
		else {
			tokens.nextToken(); // skip command label
			System.out.print("o Line: ");
			cargarUnaImagen(tokens.nextToken());
		}

	}// Fin de get Nombre de Archivo Img

	private boolean cargarUnaImagen(String directorio) {
		String nombre = getPrefix(directorio);

		if (imagesMap.containsKey(nombre)) {
			System.out.println("Error: " + nombre + "ya esta en uso");
			return false;
		}

		BufferedImage bi = CargadorImage(directorio);
		if (bi != null) {
			ArrayList imsList = new ArrayList();
			imsList.add(bi);
			imagesMap.put(nombre, imsList);
			System.out.println("  Stored " + nombre + "/" + directorio);
			return true;
		} else {
			return false;
		}

	}

	// //////////////////////////////////////////////////

	public int CargarTiraDeImagenes(String directorio, int numero) {
		String name = getPrefix(directorio);
		if (imagesMap.containsKey(name)) {
			System.out.println("Error: " + name + "ya esta en uso");
			return 0;
		}
		BufferedImage[] tira = CargarArrayTiraDeImagenes(directorio, numero);
		if (tira == null) {
			return 0;
		}
		ArrayList ImsList = new ArrayList();
		int loadCount = 0;
		System.out.print("  Agregando " + name + "/" + directorio + "... ");

		for (int i = 0; i < tira.length; i++) {
			loadCount++;
			ImsList.add(tira[i]);
			System.out.print(i + " ");
		}
		System.out.println();

		if (loadCount == 0) {
			System.out.println("No fueron cargadas las imagenes para " + name);
		} else {
			imagesMap.put(name, ImsList);
		}
		return loadCount;
	}// Fin de Cargar Tira de Imagenes

	private BufferedImage[] CargarArrayTiraDeImagenes(String directorio,
			int numero) {

		if (numero <= 0) {
			System.out.println("numero <= 0; retornando null");
			return null;
		}

		BufferedImage ImgTira;
		if ((ImgTira = CargadorImage(directorio)) == null) {
			System.out.println("Returning null");
			return null;
		}

		int imWidth = (int) ImgTira.getWidth() / numero;
		int height = ImgTira.getHeight();
		int transparency = ImgTira.getColorModel().getTransparency();

		BufferedImage[] tira = new BufferedImage[numero];
		Graphics2D tiraGC;

		for (int i = 0; i < numero; i++) {
			tira[i] = gc.createCompatibleImage(imWidth, height, transparency);
			tiraGC = tira[i].createGraphics();
			tiraGC.drawImage(ImgTira, 0, 0, imWidth, height, i * imWidth, 0,
					(i * imWidth) + imWidth, height, null);
			tiraGC.dispose();
		}
		return tira;

	}// Fin de Cargar Array Tira de Imagenes

	private BufferedImage CargadorImage(String directorio) {
		try {
			BufferedImage bf = ImageIO.read(getClass().getResource(
					IMAGE_DIR + directorio));

			int transparency = bf.getColorModel().getTransparency();
			BufferedImage copia = gc.createCompatibleImage(bf.getWidth(),
					bf.getHeight(), transparency);
			// create a graphics context
			Graphics2D g2d = copia.createGraphics();
			g2d.drawImage(bf, 0, 0, null);
			g2d.dispose();
			return copia;
		} catch (IOException e) {
			System.out.println("Error para cargar " + IMAGE_DIR + "/"
					+ directorio + ":\n" + e);
			return null;
		}
	}// Fin de cargador de Imagen

	public ArrayList getImages(String prefix) {
		ArrayList ImgList = (ArrayList) imagesMap.get(prefix);
		if (ImgList == null) {
			System.out.println("No hay imagenes guardadas en " + prefix);
			return null;
		}

		System.out.println("Guardando las imagenes en " + prefix);
		return ImgList;
	}// Fin de Get Images

	private String getPrefix(String fnm) {
		int posn;
		if ((posn = fnm.lastIndexOf(".")) == -1) {
			System.out.println("No prefix found for filename: " + fnm);
			return fnm;
		} else
			return fnm.substring(0, posn);
	} // end of getPrefix()
	public BufferedImage getImagesBuff(String name)
	{
		ArrayList imsList = (ArrayList) imagesMap.get(name);
		if (imsList == null) {
			System.out.println("Ninguna imagen guardada bajo el nombre: "
					+ name);
			return null;
		}

		
		return (BufferedImage) imsList.get(0);
	} // fin de getImagesBuff()

	public boolean estaCargado(String nombre) {
		ArrayList imsList = (ArrayList) imagesMap.get(nombre);
		if (imsList == null)
			return false;
		return true;
	} // Fin de estaCargado()

	public int numImagenes(String nombre) {
		ArrayList imsList = (ArrayList) imagesMap.get(nombre);
		if (imsList == null) {
			System.out.println("No hay imagenes guardadas bajo: " + nombre);
			return 0;
		}
		return imsList.size();
	} // Fin de numImagenes();

	public BufferedImage getImage(String name, int posn)
	{
		ArrayList imsList = (ArrayList) imagesMap.get(name);
		if (imsList == null) {
			System.out.println("No image(s) stored under " + name);
			return null;
		}

		int size = imsList.size();
		if (posn < 0) {

			return (BufferedImage) imsList.get(0); 
		} else if (posn >= size) {

			int newPosn = posn % size; // modulo

			return (BufferedImage) imsList.get(newPosn);
		}

		return (BufferedImage) imsList.get(posn);
	} // fin de getImage()

	  public BufferedImage getImagesBuff(String name, int posn)

	  {
	    ArrayList imsList = (ArrayList) imagesMap.get(name);
	    if (imsList == null) {
	      System.out.println("No image(s) stored under " + name);  
	      return null;
	    }

	    int size = imsList.size();
	    if (posn < 0) {
	      
	      
	      return (BufferedImage) imsList.get(0);  
	    }
	    else if (posn >= size) {
	     
	      int newPosn = posn % size;  
	 
	      return (BufferedImage) imsList.get(newPosn);
	    }

	 
	    return (BufferedImage) imsList.get(posn);
	  }  // end of getImage() with posn input;
	
	  
	  public BufferedImage getImage(String nombre)
	  /* Get the image associated with <name>. If there are
	     several images stored under that name, return the 
	     first one in the list.
	  */
	  {
	    ArrayList imsList = (ArrayList) imagesMap.get(nombre);
	    if (imsList == null) {
	      System.out.println("No image(s) stored under " + nombre);  
	      return null;
	    }

	    // System.out.println("Returning image stored under " + name);  
	    return (BufferedImage) imsList.get(0);
	  }  // end of getImage() with name input;

	  
	  
}
