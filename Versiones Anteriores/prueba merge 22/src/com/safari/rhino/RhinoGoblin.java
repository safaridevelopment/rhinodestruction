package com.safari.rhino;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class RhinoGoblin extends Goblin {

	private double DURACION = 0.5;

	private static final int no_salta = 0;
	private static int subiendo = 1;
	private static int gravedad = 2;
	private static int velocidad;
	private int periodo;
	private ManejadorDeLadrillos ladrillin;
	private static final int DIST_MAX_SUBIENDO = 40;

	private int movVertical;
	private int distVertical;
	private int contSubiendo;

	private boolean derecha;

	private int xMundo;

	private int yMundo;

	private boolean quieto;

	private boolean izquierda;
	// ///////working here////////

	public int posColision;

	public ManejadorDeColision colision;
	public static String ladrillos_info = "infoLadrillos.txt";
	public int posX;
	public int posY;

	// /Sonidos
	ManejadorSonidos sonidos = new ManejadorSonidos();

	public RhinoGoblin(int w, int h, int velocidadLadrillo,
			ManejadorDeLadrillos ml, ImaCharger ic, int p) {

		super(w / 2, h / 2, w, h, ic, "rhinorun");
		velocidad = velocidadLadrillo;
		ladrillin = ml;
		periodo = p;
		setPaso(0, 0);
		derecha = true;
		quieto = true;
		ylock = ladrillin.encontrarPiso(xlock + getWidth() / 2) - getHeight();
		xMundo = xlock;
		yMundo = ylock;
		movVertical = no_salta;
		distVertical = ladrillin.getLadrilloH() / 2;
		contSubiendo = 0;

	}

	//

	public void movDerecha() {
		setImagen("rhinorun");
		derecha = true;
		izquierda = false;
		quieto = false;
		sonidos.sonido("step1.wav");
	}

	public void movIzquierda() {
		setImagen("rhinoleft");
		izquierda = true;
		derecha = false;
		quieto = false;
		sonidos.sonido("step1.wav");

	}

	public void sinMovimiento() {
		izquierda = false;
		derecha = false;
		quieto = true;
	}

	public void salto() {

		if (movVertical == no_salta) {
			distVertical = subiendo;
			contSubiendo = 0;
			if (quieto) {
				if (derecha)
					setImagen("rhinorun");
				else
					setImagen("rhinoleft");
			}
		}
	}

	public boolean colisionaLadrillo() {
		if (quieto) {
			return false;
		}

		int xNuevo;

		if (derecha) {
			xNuevo = xMundo + velocidad;
		} else {
			xNuevo = xMundo - velocidad;
		}

		// Se prueba en un punto de control en la base del personaje.
		// Modificar para añadir puntos de control.
		int xControl;
		xControl = (xNuevo + getWidth() / 2);
		int yControl;
		yControl = yMundo + (int) (getHeight() * 0.8);
		
		if(ladrillin.insideLadrillo(xControl, yControl)){
			ladrillin.mueveDerecha=false;
			ladrillin.mueveIzquierda=false;
		}
		return ladrillin.insideLadrillo(xControl, yControl);
		
		

	}// Fin de las colisiones con Ladrillin

	
	
	private void checkSiCae() {
		int yTrans = ladrillin.checkTopeLadrillo(xMundo + (getWidth() / 2),
				yMundo + getHeight() + distVertical, distVertical);
		if (yTrans != 0){
			movVertical = gravedad;
		}
	}

	private void updateSubiendo() {
		if (contSubiendo == DIST_MAX_SUBIENDO) {
			distVertical = gravedad;
			contSubiendo = 0;
		} else {
			int yTrans = ladrillin.checkBaseLadrillo(xMundo + (getWidth() / 2),
					yMundo - distVertical, distVertical);
			if (yTrans == 0) {
				distVertical = gravedad;
				contSubiendo = 0;
			} else {
				trasladar(0, -yTrans);
				yMundo -= yTrans;
				contSubiendo++;
			}
		}
	}

	private void updateGravedad() {
		int yTrans = ladrillin.checkTopeLadrillo(xMundo + (getWidth() / 2),
				yMundo + getHeight() + distVertical, distVertical);
		if (yTrans == 0) {
			finalizaSalto();
		System.out.println("Colision Vertical");
		}
		else {
			trasladar(0, yTrans);
			
			yMundo += yTrans;
		}
	}

	public void finalizaSalto() {
		movVertical = no_salta;
		contSubiendo = 0;
		if (quieto) {
			if (derecha)
				setImagen("rhinorun");
			else
				setImagen("rhinoleft");
		}
	} 

	public void updateGoblin() {
		if (!quieto) {
			if (derecha)
				xMundo += velocidad;
			else if (izquierda)
				xMundo -= velocidad;
			if (movVertical == no_salta)
				checkSiCae();
		}
		if (movVertical == subiendo) {
			
			updateSubiendo();
		} else if (movVertical == gravedad)
			updateGravedad();

		super.updateGoblin();
	}

}

// public void chequearGravedad() {
// if (colision.checkColision(getposX(), getposY())) {
// // nada
// } else {
// posY = posY + gravedad;
// }
//
// }
//
// public boolean chequearParedeDerecha() {
// if (colision.checkColision(getposX() + getWidth(), getposY()
// - getHeight())) {
// posColision = getposX();
// sinMovimiento();
// sonidos.sonido("explosion.wav");
// return true;
// }
// if (colision.checkColision(getposX() + getWidth(), getposY()
// - getHeight() / 2)) {
// posColision = getposX();
// sinMovimiento();
// sonidos.sonido("explosion.wav");
// return true;
// }
// if (colision.checkColision(getposX() + getWidth(), getposY() - 1)) {
// posColision = getposX();
// sinMovimiento();
// sonidos.sonido("explosion.wav");
// return true;
// } else {
// return false;// movDerecha(true);
// }
// }
//
// public boolean chequearParedeIzquierda() {
// if (colision.checkColision(getposX(), getposY() - getHeight())) {
// posColision = getposX();
// sinMovimiento();
// sonidos.sonido("explosion.wav");
// return true;
// }
// if (colision.checkColision(getposX(), getposY() - getHeight() / 2)) {
// posColision = getposX();
// sinMovimiento();
// sonidos.sonido("explosion.wav");
// return true;
// }
// if (colision.checkColision(getposX(), getposY() - 1)) {
// posColision = getposX();
// sinMovimiento();
// sonidos.sonido("explosion.wav");
// return true;
// } else {
// return false;// movDerecha(true);
// }
//
// }
