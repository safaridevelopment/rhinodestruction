package com.safari.rhino;

import java.awt.*;
import java.awt.image.*;

public class Lazo {

	private BufferedImage im;
	private int width; // ancho de la imagen >= pWidth
	private int pWidth, pHeight; // dimensiones del panel a mostrar

	private int tamanoMov; // lo que la imagen se mueve en pixeles
	private boolean seMueveDer;
	private boolean seMueveIzq;

	private int estoyEnX;// posicion en X

	public Lazo(int w, int h, BufferedImage im, int moveSz) {

		pWidth = w;
		pHeight = h;

		this.im = im;

		width = im.getWidth();

		if (width < pWidth) {
			System.out.println("Ancho del Lazo < Ancho del panel");
		}

		tamanoMov = moveSz;
		seMueveDer = false;
		seMueveIzq = false;
		estoyEnX = 0;

	}

	// el lazo se mueve a la derecha
	public void moverDer() {
		// TODO Auto-generated method stub
		seMueveDer = true;
		seMueveIzq = false;
	}

	// el lazo se mueve a la izquierda
	public void moverIzq() {
		// TODO Auto-generated method stub
		seMueveDer = false;
		seMueveIzq = true;

	}

	// el lazo no se mueve
	public void quieto() {
		// TODO Auto-generated method stub
		seMueveDer = false;
		seMueveIzq = false;
	}

	public void update() {
		// TODO Auto-generated method stub
		if (seMueveDer) {
			estoyEnX = (estoyEnX + tamanoMov) % width;
		} else if (seMueveIzq) {
			estoyEnX = (estoyEnX - tamanoMov) % width;
		}
	}

	public void display(Graphics g) {
		if (estoyEnX == 0) {
			dibujar(g, im, 0, pWidth, 0, pWidth);
		} else if ((estoyEnX > 0) && (estoyEnX < pWidth)) {
			dibujar(g, im, 0, estoyEnX, width - estoyEnX, width);
			dibujar(g, im, estoyEnX, pWidth, 0, pWidth - estoyEnX);
		} else if (estoyEnX >= pWidth) {
			dibujar(g, im, 0, pWidth, width - estoyEnX, width - estoyEnX
					+ pWidth);
		} else if ((estoyEnX < 0) && (estoyEnX >= pWidth - width)) {
			dibujar(g, im, 0, pWidth, -estoyEnX, pWidth - estoyEnX);
		} else if (estoyEnX < pWidth - width) {
			dibujar(g, im, 0, width + estoyEnX, -estoyEnX, width);
			dibujar(g, im, width + estoyEnX, pWidth, 0, pWidth - estoyEnX
					- width);
		}

	}

	private void dibujar(Graphics g, BufferedImage im, int scrX1, int scrX2,
			int imX1, int imX2) {
		g.drawImage(im, scrX1, 0, scrX2, pHeight, imX1, 0, imX2, pHeight, null);
	}

}
