package com.safari.test;

import static org.junit.Assert.*;

import java.awt.Graphics;
import java.util.ArrayList;

import org.junit.Test;

import com.safari.rhino.ImaCharger;
import com.safari.rhino.Ladrillo;
import com.safari.rhino.ManejadorDeLadrillos;
import com.safari.rhino.RhinoPanel;

public class MapAllTest extends ImaCharger{
	// pruebas sobre Ladrillo
	@Test
	public void prueba_obtener_pos_x_centro_ladrillo_en_Ladrillo() {
		Ladrillo L = new Ladrillo(0, 0, 0);
		assertEquals(0, L.getmapX());
	}

	@Test
	public void prueba_obtener_pos_y_centro_ladrillo_en_Ladrillo() {
		Ladrillo L = new Ladrillo(0, 0, 0);
		assertEquals(0, L.getmapY());
	}

	@Test
	public void prueba_obtener_img_ID_centro_ladrillo_en_Ladrillo() {
		Ladrillo L = new Ladrillo(0, 0, 0);
		assertEquals(0, L.getimagenID());
	}
	@Test
	public void prueba_set_get_coorde_Y_en_Ladrillo() {
		Ladrillo L = new Ladrillo(0, 0, 0);
		L.setcoordY(0, 0);
		assertEquals(0, L.getcoordY());
	}
	@Test
	public void prueba_display_en_Ladrillo() {
		Ladrillo L = new Ladrillo(0, 0, 0);

	}
	// pruebas sobre ima Charger
	@Test
	public void prueba_cargarGrupoImg_desdeImaCharger() {
		ImaCharger I = new ImaCharger();
		int r;		
		assertEquals(0, I.CargarTiraDeImagenes("", 0));
	}
	@Test
	public void prueba_cargarGrupoImg_con_entrada_desdeImaCharger() {
		ImaCharger I = new ImaCharger("");
		int r;		
		assertEquals(0, I.CargarTiraDeImagenes("", 0));
	}
	@Test
	public void prueba_getImages_desdeImaCharger() {
		ImaCharger I = new ImaCharger();
		assertEquals(null, I.getImages(""));
	}
	/// Pruebas sobre Manejador de Ladrillos
//	@Test
//	public void prueba_cargarNumeroImg_desdeManejadorDeLadrillos() {
//		ImaCharger I = new ImaCharger();
//		ManejadorDeLadrillos m = new ManejadorDeLadrillos(0, 0, "", I);
//		assertFalse(false);
//		
//	}
	/// Pruebas sobre Rhino Panel
	

}