
public class Nivel4 {
	private String IMAGENES_INFO = "imsInfo_nivel4.txt";
	private String LADRILLOS_INFO = "bricksInfo_nivel4.txt";
	private int VIDA = 100;
	private int PUNT_MAX = 800;
	private int vidarest = VIDA;

	// ///LADRILLOS
	private double MOVE_FACTOR = 0.2;

	// ////MISILES
	private int PASO = -11;

	// //RHINO SPRITE

	// /LAZO
	private String fondo1 = "fondo1_nivel4";
	private String fondo2 = "fondo2_nivel4";
	private String fondo3 = "fondo3_nivel4";

	public Nivel4() {
	}

	public String getImagenes_Info() {
		return IMAGENES_INFO;
	}

	public String getLadrillos_Info() {
		return LADRILLOS_INFO;
	}

	public int getVida() {
		return VIDA;
	}

	public int getPunt_Max() {
		return PUNT_MAX;
	}

	public int getVidarest() {
		return vidarest;
	}

	public double getMove_Factor() {
		return MOVE_FACTOR;
	}

	public int getPasoMisil() {
		return PASO;
	}

	public String getFondo1() {
		return fondo1;
	}

	public String getFondo2() {
		return fondo2;
	}

	public String getFondo3() {
		return fondo3;
	}

}
