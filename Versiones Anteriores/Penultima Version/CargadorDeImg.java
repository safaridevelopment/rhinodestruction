
import java.awt.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

public class CargadorDeImg {
	private final static String IMAGE_DIR = "Images/";

	private HashMap imgMap;

	private HashMap gNamesMap;
	private GraphicsConfiguration gc;

	public CargadorDeImg(String fnm) {
		initLoader();
		cargarImgFile(fnm);
	}

	public CargadorDeImg() {
		initLoader();
	}

	private void initLoader() {
		imgMap = new HashMap();
		gNamesMap = new HashMap();

		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
	}

	private void cargarImgFile(String plano) {
		String imgPlano = IMAGE_DIR + plano;
		System.out.println("Leyendo archivo " + imgPlano);
		try {
			InputStream in = this.getClass().getResourceAsStream(imgPlano);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String linea;
			char ch;
			while ((linea = br.readLine()) != null) {
				if (linea.length() == 0)
					continue;
				if (linea.startsWith("//"))
					continue;
				ch = Character.toLowerCase(linea.charAt(0));
				if (ch == 'o')
					obtenerNombreArchivo(linea);
				else if (ch == 'n')
					getImgNumeradas(linea);
				else if (ch == 's')
					getTiraImg(linea);
				else if (ch == 'g')
					getGrupoDeImg(linea);
				else
					System.out.println("No se reconoce la linea: " + linea);
			}
			br.close();
		} catch (IOException e) {
			System.out.println("Error al leer el archivo: " + imgPlano);
			System.exit(1);
		}
	}

	private void obtenerNombreArchivo(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() != 2)
			System.out.println("Error de cantidad de argumentos " + linea);
		else {
			tokens.nextToken();
			System.out.print("o Line: ");
			carcarUnaImg(tokens.nextToken());
		}
	}

	public boolean carcarUnaImg(String plano) {
		String nombre = getPrefijo(plano);

		if (imgMap.containsKey(nombre)) {
			System.out.println("Error: " + nombre + "ya esta en uso");
			return false;
		}

		BufferedImage bi = loadImage(plano);
		if (bi != null) {
			ArrayList imsList = new ArrayList();
			imsList.add(bi);
			imgMap.put(nombre, imsList);
			System.out.println("  Guardando " + nombre + "/" + plano);
			return true;
		} else
			return false;
	}

	private String getPrefijo(String plano) {
		int posn;
		if ((posn = plano.lastIndexOf(".")) == -1) {
			System.out.println("No se encontro el prefijo: " + plano);
			return plano;
		} else
			return plano.substring(0, posn);
	}

	private void getImgNumeradas(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() != 3)
			System.out.println("Numero equivocado de lineas " + linea);
		else {
			tokens.nextToken();
			System.out.print("n Linea: ");

			String fnm = tokens.nextToken();
			int numero = -1;
			try {
				numero = Integer.parseInt(tokens.nextToken());
			} catch (Exception e) {
				System.out.println("El numero es incorrecto para linea "
						+ linea);
			}
			cargarNumeroImg(fnm, numero);
		}
	}

	public int cargarNumeroImg(String plano, int number) {
		String prefix = null;
		String postfix = null;
		int starPosn = plano.lastIndexOf("*");
		if (starPosn == -1) {
			System.out.println("No '*' en el archivo: " + plano);
			prefix = getPrefijo(plano);
		} else {
			prefix = plano.substring(0, starPosn);
			postfix = plano.substring(starPosn + 1);
		}

		if (imgMap.containsKey(prefix)) {
			System.out.println("Error: " + prefix + "ya esta en uso");
			return 0;
		}

		return cargarNumImg(prefix, postfix, number);
	}

	private int cargarNumImg(String prefix, String postfix, int numero) {
		String imgPlano;
		BufferedImage bi;
		ArrayList imsList = new ArrayList();
		int loadCount = 0;

		if (numero <= 0) {
			System.out.println("Error: Numero <= 0: " + numero);
			imgPlano = prefix + postfix;
			if ((bi = loadImage(imgPlano)) != null) {
				loadCount++;
				imsList.add(bi);
				System.out.println("  Guardando " + prefix + "/" + imgPlano);
			}
		} else {
			System.out.print("  Agregando " + prefix + "/" + prefix + "*"
					+ postfix + "... ");
			for (int i = 0; i < numero; i++) {
				imgPlano = prefix + i + postfix;
				if ((bi = loadImage(imgPlano)) != null) {
					loadCount++;
					imsList.add(bi);
					System.out.print(i + " ");
				}
			}
			System.out.println();
		}

		if (loadCount == 0)
			System.out.println("No se cargaron imagenes para  " + prefix);
		else
			imgMap.put(prefix, imsList);

		return loadCount;
	}

	private void getTiraImg(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() != 3)
			System.out.println("Error en numero de argumentos en  " + linea);
		else {
			tokens.nextToken();
			System.out.print("s Linea: ");

			String fnm = tokens.nextToken();
			int number = -1;
			try {
				number = Integer.parseInt(tokens.nextToken());
			} catch (Exception e) {
				System.out.println("Numero " + linea);
			}

			cargarTiraImg(fnm, number);
		}
	}

	public int cargarTiraImg(String plano, int numero) {
		String name = getPrefijo(plano);
		if (imgMap.containsKey(name)) {
			System.out.println("Error: " + name + "ya se uso");
			return 0;
		}

		BufferedImage[] strip = cargarTiraImgArray(plano, numero);
		if (strip == null)
			return 0;

		ArrayList imsList = new ArrayList();
		int loadCount = 0;
		System.out.print("  Agregando " + name + "/" + plano + "... ");
		for (int i = 0; i < strip.length; i++) {
			loadCount++;
			imsList.add(strip[i]);
			System.out.print(i + " ");
		}
		System.out.println();

		if (loadCount == 0)
			System.out.println("No se cargaron imagenes para " + name);
		else
			imgMap.put(name, imsList);

		return loadCount;
	}

	private void getGrupoDeImg(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);

		if (tokens.countTokens() < 3)
			System.out.println("Error en el numero de argumentos " + linea);
		else {
			tokens.nextToken();
			System.out.print("g Linea: ");

			String name = tokens.nextToken();

			ArrayList fnms = new ArrayList();
			fnms.add(tokens.nextToken());
			while (tokens.hasMoreTokens())
				fnms.add(tokens.nextToken());

			cargarGroupImg(name, fnms);
		}
	}

	public int cargarGroupImg(String nombre, ArrayList planos) {
		if (imgMap.containsKey(nombre)) {
			System.out.println("Error: " + nombre + "ya en uso");
			return 0;
		}

		if (planos.size() == 0) {
			System.out.println("List of filenames is empty");
			return 0;
		}

		BufferedImage bi;
		ArrayList nms = new ArrayList();
		ArrayList imsList = new ArrayList();
		String nm, plano;
		int cargarCuenta = 0;

		System.out.println("  Agregando a " + nombre + "...");
		System.out.print("  ");
		for (int i = 0; i < planos.size(); i++) {
			plano = (String) planos.get(i);
			nm = getPrefijo(plano);
			if ((bi = loadImage(plano)) != null) {
				cargarCuenta++;
				imsList.add(bi);
				nms.add(nm);
				System.out.print(nm + "/" + plano + " ");
			}
		}
		System.out.println();

		if (cargarCuenta == 0)
			System.out.println("No se cargaron imagenes para " + nombre);
		else {
			imgMap.put(nombre, imsList);
			gNamesMap.put(nombre, nms);
		}

		return cargarCuenta;
	}

	public int cargarGrupoDeImg(String name, String[] fnms) {
		ArrayList al = new ArrayList(Arrays.asList(fnms));
		return cargarGroupImg(name, al);
	}

	public BufferedImage getImg(String nombre) {
		ArrayList imsList = (ArrayList) imgMap.get(nombre);
		if (imsList == null) {
			System.out.println("No se cargaron imagenes " + nombre);
			return null;
		}
		return (BufferedImage) imsList.get(0);
	}

	public BufferedImage getImg(String nombre, int posn) {
		ArrayList imsList = (ArrayList) imgMap.get(nombre);
		if (imsList == null) {
			System.out.println("No se cargaron imagenes " + nombre);
			return null;
		}

		int tamaño = imsList.size();
		if (posn < 0) {
			return (BufferedImage) imsList.get(0);
		} else if (posn >= tamaño) {
			int newPosn = posn % tamaño;
			return (BufferedImage) imsList.get(newPosn);
		}
		return (BufferedImage) imsList.get(posn);
	}

	public BufferedImage getImg(String nombre, String planoPrefijo) {
		ArrayList imsList = (ArrayList) imgMap.get(nombre);
		if (imsList == null) {
			System.out.println("No se cargaron imagenes " + nombre);
			return null;
		}

		int posn = getGrupoPos(nombre, planoPrefijo);
		if (posn < 0) {
			return (BufferedImage) imsList.get(0);
		}
		return (BufferedImage) imsList.get(posn);
	}

	private int getGrupoPos(String nombre, String planoPrefijo) {
		ArrayList nombreDeGrupo = (ArrayList) gNamesMap.get(nombre);
		if (nombreDeGrupo == null) {
			System.out.println("No se cargaron imagenes " + nombre);
			return -1;
		}

		String nm;
		for (int i = 0; i < nombreDeGrupo.size(); i++) {
			nm = (String) nombreDeGrupo.get(i);
			if (nm.equals(planoPrefijo))
				return i;
		}

		System.out.println("No " + planoPrefijo + " hay grupo de imagenes "
				+ nombre);
		return -1;
	}

	public ArrayList getImages(String nombre) {
		ArrayList imsList = (ArrayList) imgMap.get(nombre);
		if (imsList == null) {
			System.out.println("No se cargaron imagenes " + nombre);
			return null;
		}

		System.out.println("Retornando todas las imagenes " + nombre);
		return imsList;
	}

	public boolean estaCargado(String nombre) {
		ArrayList imsList = (ArrayList) imgMap.get(nombre);
		if (imsList == null)
			return false;
		return true;
	}

	public int numImages(String nombre) {
		ArrayList imsList = (ArrayList) imgMap.get(nombre);
		if (imsList == null) {
			System.out.println("No se cargaron imagenes " + nombre);
			return 0;
		}
		return imsList.size();
	}

	public BufferedImage loadImage(String plano) {
		try {
			BufferedImage im = ImageIO.read(getClass().getResource(
					IMAGE_DIR + plano));

			int transparency = im.getColorModel().getTransparency();
			BufferedImage copy = gc.createCompatibleImage(im.getWidth(),
					im.getHeight(), transparency);

			Graphics2D g2d = copy.createGraphics();

			g2d.drawImage(im, 0, 0, null);
			g2d.dispose();
			return copy;
		} catch (IOException e) {
			System.out.println("Cargando ocurrio un error " + IMAGE_DIR + "/"
					+ plano + ":\n" + e);
			return null;
		}
	}

	private void reportarTransparente(String plano, int transparent) {
		System.out.print(plano + " transparency: ");
		switch (transparent) {
		case Transparency.OPAQUE:
			System.out.println("opaque");
			break;
		case Transparency.BITMASK:
			System.out.println("bitmask");
			break;
		case Transparency.TRANSLUCENT:
			System.out.println("translucent");
			break;
		default:
			System.out.println("unknown");
			break;
		}
	}

	private BufferedImage cargarImg2(String fnm) {
		ImageIcon imIcon = new ImageIcon(getClass()
				.getResource(IMAGE_DIR + fnm));
		if (imIcon == null)
			return null;

		int width = imIcon.getIconWidth();
		int height = imIcon.getIconHeight();
		Image im = imIcon.getImage();

		return construirBIM(im, width, height);
	}

	private BufferedImage construirBIM(Image im, int width, int height)

	{
		BufferedImage copy = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = copy.createGraphics();

		g2d.drawImage(im, 0, 0, null);
		g2d.dispose();
		return copy;
	}

	public BufferedImage cargarImg3(String plano) {
		Image im = leerImg(plano);
		if (im == null)
			return null;

		int width = im.getWidth(null);
		int height = im.getHeight(null);

		return construirBIM(im, width, height);
	}

	private Image leerImg(String plano) {
		Image image = Toolkit.getDefaultToolkit().getImage(
				getClass().getResource(IMAGE_DIR + plano));
		MediaTracker imageTracker = new MediaTracker(new JPanel());

		imageTracker.addImage(image, 0);
		try {
			imageTracker.waitForID(0);
		} catch (InterruptedException e) {
			return null;
		}
		if (imageTracker.isErrorID(0))
			return null;
		return image;
	}

	public BufferedImage[] cargarTiraImgArray(String fnm, int number) {
		if (number <= 0) {
			System.out.println("numero <= 0; retornando null");
			return null;
		}

		BufferedImage stripIm;
		if ((stripIm = loadImage(fnm)) == null) {
			System.out.println("Retornando null");
			return null;
		}

		int imWidth = (int) stripIm.getWidth() / number;
		int height = stripIm.getHeight();
		int transparency = stripIm.getColorModel().getTransparency();

		BufferedImage[] strip = new BufferedImage[number];
		Graphics2D stripGC;

		for (int i = 0; i < number; i++) {
			strip[i] = gc.createCompatibleImage(imWidth, height, transparency);

			stripGC = strip[i].createGraphics();

			stripGC.drawImage(stripIm, 0, 0, imWidth, height, i * imWidth, 0,
					(i * imWidth) + imWidth, height, null);
			stripGC.dispose();
		}
		return strip;
	}
}
