
import java.awt.*;

public class CoheteSprite extends Sprite {
	private static final int PASO = -10;
	private RhinoPanel rp;
	private RhinoSprite rhino;
	private AvionSprite AVION;

	public CoheteSprite(int w, int h, CargadorDeImg imsLd, RhinoPanel rp,
			RhinoSprite r, AvionSprite avion) {
		super(w, h / 2, w, h, imsLd, "cohete");
		this.rp = rp;
		rhino = r;
		AVION = avion;
		posicionInicial();
		// lanzarCohete();
	}

	private void posicionInicial() {
		setPosicion(AVION.getPosX()+5, AVION.getPosY()+5);
	}

	public void updateCoheteSprite() {
		if (rp.BANDERA_DE_AVION_DER && !rhino.quieto) {
			setPaso(-5, -PASO);
		} else {
			if (rp.BANDERA_DE_AVION_IZQ && !rhino.quieto) {
				setPaso(5, -PASO);
			} else {
				setPaso(0, -PASO);
			}
		}
		golpeoRhino();
		fueraDePantalla();
//		golpeEdif();
		super.updateRhinoSprite();
	}

	private void golpeoRhino() {
		Rectangle rectRhino = rhino.getMiRectangulo();
		rectRhino.grow(-rectRhino.width / 3, 0);
		if (rectRhino.intersects(getMiRectangulo())) {
			rp.explosion(posX, posY + getHeight());
			posicionInicial();
			rp.vida +=5;
		}
	}

	private void fueraDePantalla() {
		if (((posY + getHeight()) >= 540)){
			rp.explosionCohete(posX,posY);
			posicionInicial();
		}
	}

}