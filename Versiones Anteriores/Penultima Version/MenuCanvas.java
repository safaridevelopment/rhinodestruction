import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class MenuCanvas extends JPanel {
	private static final long serialVersionUID = 1L;
	private BufferedImage procesoBimg;

	Graphics2D g2d;

	public MenuCanvas() {
		try {
			procesoBimg = ImageIO.read(ClassLoader
					.getSystemResourceAsStream("Images/RhinoLogo.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void paint(Graphics g) {
		update(g);
	}

	public void update(Graphics g) {
		g2d = (Graphics2D) g;

		g2d.drawImage(procesoBimg, 10, 10, null);

	}

	public void publicarInstrucciones() {
		g2d.setColor(Color.white);
		g2d.setFont(new Font("Verdana", Font.BOLD, 30));
		g2d.drawString("Hola Mundo", 100, 200);

		paint(g2d);

	}

}