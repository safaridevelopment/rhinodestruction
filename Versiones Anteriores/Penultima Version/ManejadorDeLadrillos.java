import java.awt.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

public class ManejadorDeLadrillos {
	private final static String IMAGENES_DIR = "Images/";
	private final static int MAX_LINEAS_LADRILLOS = 15;

	private final static double MOVE_FACTOR = 0.2;

	private int panel_Width, panel_Height;
	private int width, height;

	private int ladrilloWidth, ladrilloHeight;
	private int numColumnas, numFilas;

	private int velocidad;

	public int puntaje = 0;
	public int vida = 0;

	private boolean mueveDerecha;
	private boolean mueveIzquierda;

	public boolean punching_test = false;

	private int xMapHead;

	private ArrayList listaLadrillos;

	private ArrayList[] columnaLadrillos;

	private CargadorDeImg imsLoader;
	private ArrayList ladrilloImages = null;

	public ManejadorDeLadrillos(int w, int h, String fnm, CargadorDeImg il) {
		panel_Width = w;
		panel_Height = h;
		imsLoader = il;

		listaLadrillos = new ArrayList();
		cargarArchivoLadrillos(fnm);
		initInfoLadrillos();
		crearColumnas();

		velocidad = (int) (ladrilloWidth * MOVE_FACTOR);
		System.out.println("Velocidad: " + velocidad);
		if (velocidad == 0) {
			System.out
					.println("La Velocidad no puede ser 0, se establecera en 1");
			velocidad = 1;
		}

		mueveDerecha = false;
		mueveIzquierda = false;
		xMapHead = 0;
	}

	private void cargarArchivoLadrillos(String plano) {
		String imsFNm = IMAGENES_DIR + plano;
		System.out.println("Leyendo archivo de Ladrillos: " + imsFNm);

		int numTiraImagenes = -1;
		int numLineasLadrillos = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(imsFNm));
			String linea;
			char ch;
			while ((linea = br.readLine()) != null) {
				if (linea.length() == 0)
					continue;
				if (linea.startsWith("//"))
					continue;
				ch = Character.toLowerCase(linea.charAt(0));
				if (ch == 's')
					numTiraImagenes = obtenerTiraImagenes(linea);
				else {
					if (numLineasLadrillos > MAX_LINEAS_LADRILLOS)
						System.out
								.println("Max alcanzado, volandose las lineas de ladrillos: "
										+ linea);
					else if (numTiraImagenes == -1)
						System.out
								.println("No hay tira de imagenes, volandose las linas de ladrillos: "
										+ linea);
					else {
						guardarLadrillos(linea, numLineasLadrillos,
								numTiraImagenes);
						numLineasLadrillos++;
					}
				}
			}
			br.close();
		} catch (IOException e) {
			System.out.println("Error Leyendo Archivo: " + imsFNm);
			System.exit(1);
		}
	}

	private int obtenerTiraImagenes(String linea) {
		StringTokenizer tokens = new StringTokenizer(linea);
		if (tokens.countTokens() != 3) {
			System.out.println("No hay argumentos para " + linea);
			return -1;
		} else {
			tokens.nextToken();
			System.out.print("Tira Ladrillos: ");

			String plano = tokens.nextToken();
			int numero = -1;
			try {
				numero = Integer.parseInt(tokens.nextToken());
				imsLoader.cargarTiraImg(plano, numero);
				ladrilloImages = imsLoader.getImages(obtenerPrefijo(plano));
			} catch (Exception e) {
				System.out.println("Numero incorrecto para " + linea);
			}

			return numero;
		}
	}

	private String obtenerPrefijo(String plano) {
		int posn;
		if ((posn = plano.lastIndexOf(".")) == -1) {
			System.out.println("No se encontro prefijo para: " + plano);
			return plano;
		} else
			return plano.substring(0, posn);
	}

	private void guardarLadrillos(String linea, int lineaNo, int numImagenes) {
		int imageID;
		for (int x = 0; x < linea.length(); x++) {
			char ch = linea.charAt(x);
			if (ch == ' ')
				continue;
			if (Character.isDigit(ch)) {
				imageID = ch - '0';
				if (imageID >= numImagenes)
					System.out.println("ID de Imagen " + imageID
							+ " fuera de rango");
				else
					listaLadrillos.add(new Ladrillo(imageID, x, lineaNo));
			} else
				System.out.println("Ladrillo char " + ch + " no es un digito");
		}
	}

	private void initInfoLadrillos() {
		if (ladrilloImages == null) {
			System.out.println("No se cargaron imagenes de Ladrillos");
			System.exit(1);
		}
		if (listaLadrillos.size() == 0) {
			System.out.println("No se cargo un plano de ladrillos");
			System.exit(1);
		}

		BufferedImage im = (BufferedImage) ladrilloImages.get(0);
		ladrilloWidth = im.getWidth();
		ladrilloHeight = im.getHeight();

		encontrarNumeroLadrillos();
		calcDimensionesMapa();
		checkearVacios();

		addDetallesLadrillos();
	}

	private void encontrarNumeroLadrillos() {
		Ladrillo b;
		numColumnas = 0;
		numFilas = 0;
		for (int i = 0; i < listaLadrillos.size(); i++) {
			b = (Ladrillo) listaLadrillos.get(i);
			if (numColumnas < b.getMapX())
				numColumnas = b.getMapX();
			if (numFilas < b.getMapY())
				numFilas = b.getMapY();
		}
		numColumnas++;
		numFilas++;
	}

	private void calcDimensionesMapa() {
		width = ladrilloWidth * numColumnas;
		height = ladrilloHeight * numFilas;

		if (width < panel_Width) {
			System.out
					.println("El mapa de Ladrillos es menos ancho que el Panel");
			System.exit(0);
		}
	}

	private void checkearVacios() {
		boolean[] tieneLadrillo = new boolean[numColumnas];
		for (int j = 0; j < numColumnas; j++)
			tieneLadrillo[j] = false;

		Ladrillo b;
		for (int i = 0; i < listaLadrillos.size(); i++) {
			b = (Ladrillo) listaLadrillos.get(i);
			if (b.getMapY() == numFilas - 1)
				tieneLadrillo[b.getMapX()] = true;
		}

		for (int j = 0; j < numColumnas; j++)
			if (!tieneLadrillo[j]) {
				System.out
						.println("Vacio encontrado en el mapa de ladrillos al fondo, en la linea: "
								+ j);
				System.exit(0);
			}
	}

	private void addDetallesLadrillos() {
		Ladrillo b;
		BufferedImage im;
		for (int i = 0; i < listaLadrillos.size(); i++) {
			b = (Ladrillo) listaLadrillos.get(i);
			im = (BufferedImage) ladrilloImages.get(b.getImageID());
			b.setImage(im);
			b.setLocY(panel_Height, numFilas);
		}
	}

	private void crearColumnas() {
		columnaLadrillos = new ArrayList[numColumnas];
		for (int i = 0; i < numColumnas; i++)
			columnaLadrillos[i] = new ArrayList();
		Ladrillo b;
		for (int j = 0; j < listaLadrillos.size(); j++) {
			b = (Ladrillo) listaLadrillos.get(j);
			columnaLadrillos[b.getMapX()].add(b);

		}
	}

	public void moverDerecha() {
		mueveDerecha = true;
		mueveIzquierda = false;
	}

	public void moverIzquierda() {
		mueveDerecha = false;
		mueveIzquierda = true;
	}

	public void quieto() {
		mueveDerecha = false;
		mueveIzquierda = false;
	}

	public int getXMapHead() {
		return xMapHead;
	}

	public void update() {
		if (mueveDerecha)
			xMapHead = (xMapHead + velocidad) % width;
		else if (mueveIzquierda)
			xMapHead = (xMapHead - velocidad) % width;

	}

	public void display(Graphics g) {
		// addDetallesLadrillos();
		int bCoord = (int) (xMapHead / ladrilloWidth) * ladrilloWidth;

		int offset;
		if (bCoord >= 0)
			offset = xMapHead - bCoord;
		else

			offset = bCoord - xMapHead;

		if ((bCoord >= 0) && (bCoord < panel_Width)) {
			dibujarLadrillos(g, 0 - (ladrilloWidth - offset), xMapHead, width
					- bCoord - ladrilloWidth);
			dibujarLadrillos(g, xMapHead, panel_Width, 0);
		} else if (bCoord >= panel_Width)
			dibujarLadrillos(g, 0 - (ladrilloWidth - offset), panel_Width,
					width - bCoord - ladrilloWidth);
		else if ((bCoord < 0)
				&& (bCoord >= panel_Width - width + ladrilloWidth))
			dibujarLadrillos(g, 0 - offset, panel_Width, -bCoord);
		else if (bCoord < panel_Width - width + ladrilloWidth) {
			dibujarLadrillos(g, 0 - offset, width + xMapHead, -bCoord);
			dibujarLadrillos(g, width + xMapHead, panel_Width, 0);
		}
	}

	private void dibujarLadrillos(Graphics g, int xStart, int xEnd,
			int xLadrillos) {
		int xMap = xLadrillos / ladrilloWidth;
		ArrayList columna;
		Ladrillo b;
		for (int x = xStart; x < xEnd; x += ladrilloWidth) {
			columna = columnaLadrillos[xMap];
			for (int i = 0; i < columna.size(); i++) {
				b = (Ladrillo) columna.get(i);
				b.display(g, x);
			}
			xMap++;
		}
	}

	public int getLadrilloHeight() {
		return ladrilloHeight;
	}

	public int encontrarPiso(int xSprite) {
		int xMap = (int) (xSprite / ladrilloWidth);

		int locY = panel_Height;
		ArrayList column = columnaLadrillos[xMap];

		Ladrillo b;
		for (int i = 0; i < column.size(); i++) {
			b = (Ladrillo) column.get(i);
			if (b.getLocY() < locY)
				locY = b.getLocY();
		}
		return locY;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public boolean dentroDeLadrillo(int xMundo, int yMundo) {
		Point mapCoord = mundoToMapa(xMundo, yMundo);
		ArrayList columna = columnaLadrillos[mapCoord.x];

		Ladrillo b;
		for (int i = 0; i < columna.size(); i++) {
			b = (Ladrillo) columna.get(i);

			if (mapCoord.y == b.getMapY()) {
				return true;
			}
		}

		return false;
	}

	public int destruirLadrillo(int xMundo, int yMundo) {
		Point mapCoord = mundoToMapa(xMundo, yMundo);
		ArrayList columna = columnaLadrillos[mapCoord.x];
		Ladrillo ventana, puerta, techo, tipo;
		Ladrillo b;
		BufferedImage im;
		im = (BufferedImage) ladrilloImages.get(5);
		ventana = new Ladrillo(5, mapCoord.x, mapCoord.y);
		ventana.setImage(im);
		ventana.setLocY(panel_Height, numFilas);

		im = (BufferedImage) ladrilloImages.get(4);
		puerta = new Ladrillo(4, mapCoord.x, mapCoord.y);
		puerta.setImage(im);
		puerta.setLocY(panel_Height, numFilas);

		im = (BufferedImage) ladrilloImages.get(6);
		techo = new Ladrillo(6, mapCoord.x, mapCoord.y);
		techo.setImage(im);
		techo.setLocY(panel_Height, numFilas);

		im = (BufferedImage) ladrilloImages.get(7);
		tipo = new Ladrillo(7, mapCoord.x, mapCoord.y);
		tipo.setImage(im);
		tipo.setLocY(panel_Height, numFilas);
		for (int i = 0; i < columna.size(); i++) {
			int rand = (int) (20 * Math.random());
			b = (Ladrillo) columna.get(i);

			if (mapCoord.y == b.getMapY()) {
				if (b.getImageID() == 2) {
					columna.remove(i);
					columna.add(i, ventana);
				}
				if (b.getImageID() == 1) {
					columna.remove(i);
					columna.add(i, puerta);
				}

				if (b.getImageID() == 3) {
					columna.remove(i);
					columna.add(i, techo);
				}

				if (b.getImageID() == 2 && (rand == 2 || rand == 3)) {
					columna.remove(i);
					columna.add(i, tipo);
				}

				if (b.getImageID() == 7) {
					columna.remove(i);
					columna.add(i, ventana);
					vida += 5;
					return vida;
				}

				if (b.getImageID() == 4 || b.getImageID() == 5
						|| b.getImageID() == 6) {
					columna.remove(i);
					puntaje += 5;
					return puntaje;
				}

			}
		}
//		vida = 0;
		return puntaje;
	}

	private Point mundoToMapa(int xMundo, int yMundo) {
		// System.out.println("Mundo: " + xMundo + ", " + yMundo);

		xMundo = xMundo % width;
		if (xMundo < 0)
			xMundo += width;
		int mapX = (int) (xMundo / ladrilloWidth);

		yMundo = yMundo - (panel_Height - height);
		int mapY = (int) (yMundo / ladrilloHeight);

		if (yMundo < 0)
			mapY = mapY - 1;

		// System.out.println("Map: " + mapX + ", " + mapY);
		return new Point(mapX, mapY);
	}

	public int checkBaseLadrillo(int xMundo, int yMundo, int paso) {
		if (dentroDeLadrillo(xMundo, yMundo)) {
			int yMapWorld = yMundo - (panel_Height - height);
			int mapY = (int) (yMapWorld / ladrilloHeight);
			int topOffset = yMapWorld - (mapY * ladrilloHeight);
			int smallStep = paso - (ladrilloHeight - topOffset);
			return smallStep;
		}
		return paso;
	}

	public int checkTopeLadrillo(int xMundo, int yMundo, int paso) {
		if (dentroDeLadrillo(xMundo, yMundo)) {
			int yMapWorld = yMundo - (panel_Height - height);
			int mapY = (int) (yMapWorld / ladrilloHeight);
			int topOffset = yMapWorld - (mapY * ladrilloHeight);
			int smallStep = paso - topOffset;
			return smallStep;
		}
		return paso;
	}

}
