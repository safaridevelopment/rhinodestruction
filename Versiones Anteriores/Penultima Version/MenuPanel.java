import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class MenuPanel extends JPanel {
	private int nivel = 0;
	private boolean tryAgain = false;
	private EventListenerList eventListenerList = new EventListenerList();

	private ActionListenerProxy actionListenerProxy = new ActionListenerProxy(
			eventListenerList);

	Font fuente = new Font("Arial", Font.BOLD, 20);

	RhinoDestruction frame;

	// --------------------------------------------------------------------------------

	public MenuPanel(RhinoDestruction frm) {

		initGUI();
		setBackground(Color.BLACK);
		frame = frm;
	}

	private void initGUI() {

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		// /////////CANVAS
		final MenuCanvas canvas = new MenuCanvas();
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 4;
		gbc.gridheight = 6;
		gbc.weightx = 4;
		gbc.weighty = 4;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		add(canvas, gbc);

		// //////////BOTONES
		JButton btnNvl1 = new JButton("", new ImageIcon(getClass().getResource(
				"Images/nivel1.png")));
		btnNvl1.setBackground(Color.black);
		btnNvl1.setOpaque(false);
		btnNvl1.setBorder(null);
		btnNvl1.setFont(fuente);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnNvl1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nivel = 1;
				btnNivelClicked();
			}
		});
		add(btnNvl1, gbc);

		JButton btnNvl2 = new JButton("", new ImageIcon(getClass().getResource(
				"Images/nivel2.png")));
		btnNvl2.setBackground(Color.black);
		btnNvl2.setOpaque(false);
		btnNvl2.setBorder(null);
		btnNvl2.setFont(fuente);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnNvl2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nivel = 2;
				btnNivelClicked();
			}
		});
		add(btnNvl2, gbc);

		JButton btnNvl3 = new JButton("", new ImageIcon(getClass().getResource(
				"Images/nivel3.png")));
		btnNvl3.setBackground(Color.black);
		btnNvl3.setOpaque(false);
		btnNvl3.setBorder(null);
		btnNvl3.setFont(fuente);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);

		btnNvl3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nivel = 3;
				btnNivelClicked();
			}
		});
		add(btnNvl3, gbc);

		JButton btnNvl4 = new JButton("", new ImageIcon(getClass().getResource(
				"Images/nivel4.png")));
		btnNvl4.setBackground(Color.black);
		btnNvl4.setOpaque(false);
		btnNvl4.setBorder(null);
		btnNvl4.setFont(fuente);
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnNvl4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nivel = 4;
				btnNivelClicked();
			}
		});
		add(btnNvl4, gbc);

		JButton btnNvl5 = new JButton("", new ImageIcon(getClass().getResource(
				"Images/nivel5.png")));
		btnNvl5.setBackground(Color.black);
		btnNvl5.setOpaque(false);
		btnNvl5.setBorder(null);
		btnNvl5.setFont(fuente);
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnNvl5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nivel = 5;
				btnNivelClicked();
			}
		});
		add(btnNvl5, gbc);

		JButton btnInstrucciones = new JButton("", new ImageIcon(getClass()
				.getResource("Images/instrucciones.png")));
		btnInstrucciones.setBackground(Color.black);
		btnInstrucciones.setOpaque(false);
		btnInstrucciones.setBorder(null);
		btnInstrucciones.setFont(fuente);
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnInstrucciones.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canvas.publicarInstrucciones();
			}
		});
		add(btnInstrucciones, gbc);

		JButton btnInfo = new JButton("", new ImageIcon(getClass().getResource(
				"Images/info.png")));
		btnInfo.setBackground(Color.black);
		btnInfo.setOpaque(false);
		btnInfo.setBorder(null);
		btnInfo.setFont(fuente);
		gbc.gridx = 2;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnInfo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		add(btnInfo, gbc);

		JButton btnSalir = new JButton("", new ImageIcon(getClass()
				.getResource("Images/salir.png")));
		btnSalir.setToolTipText("Boton de la izquierda");
		btnSalir.setBackground(Color.black);
		btnSalir.setOpaque(false);
		btnSalir.setBorder(null);
		btnSalir.setFont(fuente);
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.insets = new Insets(3, 3, 3, 3);
		btnSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		add(btnSalir, gbc);
	}

	private void btnNivelClicked() {

		ActionEvent evt = new ActionEvent(this, 0, null);
		getActionListenerProxy().fireActionEvent(evt);
	}

	public ActionListenerProxy getActionListenerProxy() {
		return actionListenerProxy;
	}

	public int getNivel() {
		return nivel;
	}

	public boolean getTryAgain() {
		return tryAgain;
	}

	public void setTryAgain(boolean tryAgain) {
		this.tryAgain = tryAgain;
	}

}