
import java.awt.Rectangle;

public class TankSprite extends Sprite {
	private static final int PASO = 8;
	private static final int STEP_OFFSET = 2;
	private RhinoPanel rp;
	private RhinoSprite rhino;

	public TankSprite(int w, int h, CargadorDeImg imsLd, RhinoPanel rp,
			RhinoSprite r) {
		super(w, h / 2, w, h, imsLd, "tank");
		this.rp = rp;
		rhino = r;
		posicionInicial();
	}

	private void posicionInicial() {
		int h = 505;
		if (h + getHeight() > getPHeight() - 40)
			h -= getHeight();
		setPosicion(-getWidth(), h);
		setPaso(PASO + getRandRange(STEP_OFFSET), 0);
	}

	private int getRandRange(int x) {
		return ((int) (2 * x * Math.random())) - x;
	}

	public void updateTankSprite() {
		// golpeoRhino();
		fueraDePantalla();
		super.updateRhinoSprite();
	}

	private void golpeoRhino() {
		Rectangle rectRhino = rhino.getMiRectangulo();
		rectRhino.grow(-rectRhino.width / 3, 0);
		if (rectRhino.intersects(getMiRectangulo())) {
			rp.explosion(posX, posY + getHeight() / 2);
			posicionInicial();
		}
	}

	private void fueraDePantalla() {
		if (((-posX + getWidth() * 15) <= 0) && (dx > 0))
			posicionInicial();
	}
}
