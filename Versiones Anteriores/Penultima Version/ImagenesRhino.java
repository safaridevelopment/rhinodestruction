
import java.awt.image.*;

public class ImagenesRhino {
	private String imNombre;
	private boolean seRepite, ticksIgnorados;
	private CargadorDeImg imsLoader;

	private int animPeriodo;

	private long animTotalTime;

	private int mostrarPeriodo;

	private double seqDuracion;

	private int numImagenes;
	private int imPosition;

	private ObservadorImagenesRhino watcher = null;

	public ImagenesRhino(String nm, int ap, double d, boolean isr,
			CargadorDeImg il) {
		imNombre = nm;
		animPeriodo = ap;
		seqDuracion = d;
		seRepite = isr;
		imsLoader = il;

		animTotalTime = 0L;

		if (seqDuracion < 0.5) {
			System.out
					.println("Pendiente, la minima duracion de la secuencia es  0.5 sec.");
			seqDuracion = 0.5;
		}

		if (!imsLoader.estaCargado(imNombre)) {
			System.out.println(imNombre + " no es conocido por CargadorDeImg");
			numImagenes = 0;
			imPosition = -1;
			ticksIgnorados = true;
		} else {
			numImagenes = imsLoader.numImages(imNombre);
			imPosition = 0;
			ticksIgnorados = false;
			mostrarPeriodo = (int) (1000 * seqDuracion / numImagenes);
		}
	}

	public void updateTick() {
		if (!ticksIgnorados) {
			animTotalTime = (animTotalTime + animPeriodo)
					% (long) (1000 * seqDuracion);

			imPosition = (int) (animTotalTime / mostrarPeriodo);
			if ((imPosition == numImagenes - 1) && (!seRepite)) {
				ticksIgnorados = true;
				if (watcher != null)
					watcher.sequenceEnded(imNombre);
			}
		}
	}

	public BufferedImage getCurrentImage() {
		if (numImagenes != 0)
			return imsLoader.getImg(imNombre, imPosition);
		else
			return null;
	}

	public int getCurrentPosition() {
		return imPosition;
	}

	public void setWatcher(ObservadorImagenesRhino w) {
		watcher = w;
	}

	public void stop() {
		ticksIgnorados = true;
	}

	public boolean isStopped() {
		return ticksIgnorados;
	}

	public boolean atSequenceEnd() {
		return ((imPosition == numImagenes - 1) && (!seRepite));
	}

	public void restartAt(int imPosn) {
		if (numImagenes != 0) {
			if ((imPosn < 0) || (imPosn > numImagenes - 1)) {
				System.out
						.println("Fuera de rango, reiniciando comenzando de 0");
				imPosn = 0;
			}

			imPosition = imPosn;
			animTotalTime = (long) imPosition * mostrarPeriodo;
			ticksIgnorados = false;
		}
	}

	public void resume() {
		if (numImagenes != 0)
			ticksIgnorados = false;
	}

}
