import javax.swing.*;
import java.awt.event.*;

public class RhinoDestruction extends JFrame {
	private static final long serialVersionUID = 1L;
	private static int DEFAULT_FPS = 30;
	long period = (long) 1000.0 / DEFAULT_FPS;
	public boolean nvl3 = false;
	public boolean nvl2 = false;
	public boolean nvl4 = false;
	public boolean nvl5 = false;
	public boolean nvl1 = false;
	public boolean tryAgain = false;
	public RhinoDestruction() {
		super("RhinoDestruction");
		// RhinoPanel jp= new RhinoPanel(this, period*1000000L);

		pasarAMenu();
		// pasarAJuego();
		setResizable(false);
		setVisible(true);
		// getContentPane().add(jp);
		// pack();
	}

	private void pasarAJuego() {

		getContentPane().removeAll();

		final RhinoPanel jp = new RhinoPanel(this, (period * 1000000L), 1);

		jp.getActionListenerProxy().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jp.isIrAMenu() && !jp.ganador && !nvl2 && !tryAgain
						&& !nvl1)
					pasarAMenu();
				else if (!jp.isIrAMenu() && tryAgain && nvl1 && !jp.ganador)
					pasarAJuego();
				else if (nvl2 && jp.ganador && jp.nivel == 1)
					pasarAJuego_nivel2();
				else
					pasarAMenu();
			}
		});
		getContentPane().add(jp);
		jp.requestFocus();
		// jp.grabFocus();
		revalidate();

		pack();

	}

	private void pasarAJuego_nivel2() {

		getContentPane().removeAll();

		final RhinoPanel jp = new RhinoPanel(this, (period * 1000000L), 2);

		jp.getActionListenerProxy().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jp.isIrAMenu() && !jp.ganador && !nvl3 && !tryAgain
						&& !nvl2)
					pasarAMenu();
				else if (!jp.isIrAMenu() && tryAgain && nvl2 && !jp.ganador)
					pasarAJuego_nivel2();
				else if (nvl3 && jp.ganador && jp.nivel == 2)
					pasarAJuego_nivel3();
				else
					pasarAMenu();
			}
		});
		getContentPane().add(jp);
		jp.requestFocus();
		// jp.grabFocus();
		revalidate();

		pack();

	}

	private void pasarAJuego_nivel3() {

		getContentPane().removeAll();

		final RhinoPanel jp = new RhinoPanel(this, (period * 1000000L), 3);

		jp.getActionListenerProxy().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jp.isIrAMenu() && !jp.ganador && !nvl4 && !tryAgain
						&& !nvl3)
					pasarAMenu();
				else if (!jp.isIrAMenu() && tryAgain && nvl3 && !jp.ganador)
					pasarAJuego_nivel3();
				else if (nvl4 && jp.ganador && jp.nivel == 3)
					pasarAJuego_nivel4();
				else
					pasarAMenu();
			}
		});
		getContentPane().add(jp);
		jp.requestFocus();
		// jp.grabFocus();
		revalidate();

		pack();

	}

	private void pasarAJuego_nivel4() {
		getContentPane().removeAll();

		final RhinoPanel jp = new RhinoPanel(this, (period * 1000000L), 4);

		jp.getActionListenerProxy().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jp.isIrAMenu() && !jp.ganador && !nvl5 && !tryAgain
						&& !nvl4)
					pasarAMenu();
				else if (!jp.isIrAMenu() && tryAgain && nvl4 && !jp.ganador)
					pasarAJuego_nivel4();
				else if (nvl5 && jp.ganador && jp.nivel == 4)
					pasarAJuego_nivel5();
				else
					pasarAMenu();
			}
		});
		getContentPane().add(jp);
		jp.requestFocus();
		// jp.grabFocus();
		revalidate();

		pack();

	}

	private void pasarAJuego_nivel5() {
		getContentPane().removeAll();

		final RhinoPanel jp = new RhinoPanel(this, (period * 1000000L), 5);

		jp.getActionListenerProxy().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jp.isIrAMenu() && !jp.ganador && !nvl5 && !tryAgain
						&& !nvl4)
					pasarAMenu();
				else if (!jp.isIrAMenu() && tryAgain && nvl5 && !jp.ganador)
					pasarAJuego_nivel5();
				else if (jp.ganador && jp.nivel == 5)
					pasarAMenu();
				else
					pasarAMenu();
			}
		});
		getContentPane().add(jp);
		jp.requestFocus();
		// jp.grabFocus();
		revalidate();

		pack();

	}

	public void pasarAMenu() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(800, 600);
		getContentPane().removeAll();

		final MenuPanel menuPanel = new MenuPanel(this);
		menuPanel.getActionListenerProxy().addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (menuPanel.getNivel() == 1)
							pasarAJuego();
						if (menuPanel.getNivel() == 2) {
							pasarAJuego_nivel2();

						}
						if (menuPanel.getNivel() == 3)
							pasarAJuego_nivel3();
						if (menuPanel.getNivel() == 4)
							pasarAJuego_nivel4();
						if (menuPanel.getNivel() == 5)
							pasarAJuego_nivel5();
					}
				});
		getContentPane().add(menuPanel);
		revalidate();
	}

	public static void main(String args[]) {

		new RhinoDestruction();
	}
}
