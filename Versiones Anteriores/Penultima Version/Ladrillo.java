

import java.awt.*;
import java.awt.image.*;

public class Ladrillo {
	private int mapX, mapY;
	private int imageID;

	private BufferedImage image;
	private int height;

	private int locY;

	public Ladrillo(int id, int x, int y) {
		mapX = x;
		mapY = y;
		imageID = id;
	}

	public int getMapX() {
		return mapX;
	}

	public int getMapY() {
		return mapY;
	}

	public int getImageID() {
		return imageID;
	}

	public void setImage(BufferedImage im) {
		image = im;
		height = im.getHeight();
	}

	public void setLocY(int pHeight, int maxYBricks) {
		locY = pHeight - ((maxYBricks - mapY) * height);
	}

	public int getLocY() {
		return locY;
	}

	public void display(Graphics g, int xScr)
	{
		g.drawImage(image, xScr, locY, null);
	}

}
