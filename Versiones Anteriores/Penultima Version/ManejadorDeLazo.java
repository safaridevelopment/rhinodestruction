
import java.awt.*;

public class ManejadorDeLazo {
	//private String imagenesLazo[];
	private double factorDeMovimiento[] = { 0.1, 0.5, 1.0 };

	private Lazo[] lazos;
	private int cantidadLazos;
	private int tamannoMovimiento;

	public ManejadorDeLazo(int ancho, int alto, int ladrilloMovimiento,
			CargadorDeImg cargadorImagenes, String f1, String f2, String f3) {
		
		String imagenesLazo[]={f1,f2,f3};
		tamannoMovimiento = ladrilloMovimiento;
		cantidadLazos = imagenesLazo.length;
		lazos = new Lazo[cantidadLazos];

		for (int i = 0; i < cantidadLazos; i++)
			lazos[i] = new Lazo(ancho, alto,
					cargadorImagenes.getImg(imagenesLazo[i]),
					(int) (factorDeMovimiento[i] * tamannoMovimiento));
	}

	public void moverDerecha() {
		for (int i = 0; i < cantidadLazos; i++)
			lazos[i].moverHaciaDerecha();
	}

	public void moverIzquierda() {
		for (int i = 0; i < cantidadLazos; i++)
			lazos[i].moverHaciaIzquierda();
	}

	public void moverNo() {
		for (int i = 0; i < cantidadLazos; i++)
			lazos[i].sinMovimiento();
	}

	public void actualizar() {
		for (int i = 0; i < cantidadLazos; i++)
			lazos[i].actualizar();
	}

	public void display(Graphics g) {
		for (int i = 0; i < cantidadLazos; i++)
			lazos[i].pantalla(g);
	}

}
