
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import sun.audio.AudioData;
import sun.audio.AudioDataStream;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class ManejadorDeSonido {

	public AudioPlayer ap = AudioPlayer.player;
	public AudioStream as;
	public AudioData ad;
	
	public void sonido(String soundfile) {
		try {
			as = new AudioStream(new FileInputStream("Sonidos/"+soundfile));
			ad = as.getData();
			AudioDataStream clip = new AudioDataStream(ad);
			ap.start(clip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
}
