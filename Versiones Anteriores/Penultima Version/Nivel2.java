
public class Nivel2 {
	private String IMAGENES_INFO = "imsInfo_nivel2.txt";
	private String LADRILLOS_INFO = "bricksInfo_nivel2.txt";
	private int VIDA = 100;
	private int PUNT_MAX = 600;
	private int vidarest = VIDA;

	// ///LADRILLOS
	private double MOVE_FACTOR = 0.2;

	// ////MISILES
	public int PASO = -12;

	// //RHINO SPRITE

	// /LAZO
	private String fondo1 = "fondo1_nivel2";
	private String fondo2 = "fondo2_nivel2";
	private String fondo3 = "fondo3_nivel2";

	public Nivel2() {
	}

	public String getImagenes_Info() {
		return IMAGENES_INFO;
	}

	public String getLadrillos_Info() {
		return LADRILLOS_INFO;
	}

	public int getVida() {
		return VIDA;
	}

	public int getPunt_Max() {
		return PUNT_MAX;
	}

	public int getVidarest() {
		return vidarest;
	}

	public double getMove_Factor() {
		return MOVE_FACTOR;
	}

	public int getPasoMisil() {
		return PASO;
	}

	public String getFondo1() {
		return fondo1;
	}

	public String getFondo2() {
		return fondo2;
	}

	public String getFondo3() {
		return fondo3;
	}

}
