
import java.awt.*;

public class MisilSprite extends Sprite {
	private int PASO;
	private static final int STEP_OFFSET = 2;
	private RhinoPanel rp;
	private RhinoSprite rhino;

	public MisilSprite(int w, int h, CargadorDeImg imsLd, RhinoPanel rp,
			RhinoSprite r, int niv, int step) {
		super(w, h / 2, w, h, imsLd, "misil");
		PASO=step;

		this.rp = rp;
		rhino = r;
		posicionInicial();
	}

	private void posicionInicial() {
		int h = getPHeight() / 3 + ((int) (getPHeight() * Math.random())/2);
		if (h + getHeight() > getPHeight()- 40)
			h -= getHeight();
		setPosicion(getPWidth(), h);
		setPaso(PASO + getRandRange(STEP_OFFSET), 0);
	}

	private int getRandRange(int x) {
		return ((int) (2 * x * Math.random())) - x;
	}

	public void updateMisilSprite() {
		golpeoRhino();
		fueraDePantalla();
		super.updateRhinoSprite();
	}

	private void golpeoRhino() {
		Rectangle rectRhino = rhino.getMiRectangulo();
		rectRhino.grow(-rectRhino.width / 3, 0);
		if (rectRhino.intersects(getMiRectangulo())) {
			rp.explosion(posX, posY + getHeight() / 2);
			posicionInicial();
			rp.vida +=5;
		}
	}

	private void fueraDePantalla() {
		if (((posX + getWidth()) <= 0) && (dx < 0))
			posicionInicial();
	}

}