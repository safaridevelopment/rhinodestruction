import javax.swing.*;
import javax.swing.event.EventListenerList;
import sun.awt.windows.ThemeReader;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.*;
import com.sun.j3d.utils.timer.J3DTimer;

public class RhinoPanel extends JPanel implements Runnable,
		ObservadorImagenesRhino {
	private static final long serialVersionUID = 1L;
	private static final int PANEL_WIDTH = 800;
	private static final int PANEL_HEIGHT = 600;
	private int VIDA;
	private static final int NO_DELAYS_PER_YIELD = 16;

	private static final int MAX_FRAME_SKIPS = 5;

	// /////////VARIABLES VARIANTES POR CADA NIVEL
	private int PUNT_MAX;
	private String IMAGENES_INFO;
	private String LADRILLOS_INFO;
	public int nivel = 0;
	public int tempmax = 120;
	private int vidarest;
	private int pasoMisil;
	// ////////////////////

	private Thread animator;
	private volatile boolean running = false;
	public boolean gameOver = false;
	public boolean ganador = false;
	private long periodo;
	private boolean salir = false;

	private int tiempoEnJuego;
	private Font msgFont, msgFontGO, msgFontPaused;
	private FontMetrics metrics;

	private RhinoDestruction rhinoTop;

	private RhinoSprite rhino;
	private MisilSprite misil;

	private MisilSprite misil2;

	private AvionSprite AVION;
	private CoheteSprite COHETE;
	private TankSprite tank;
	private MisilDeTanque tankMisil;

	private ManejadorDeLazo lazin;
	private ManejadorDeLadrillos ladrillin;

	private long gameStartTime, iniciaTiempoEnJuego;

	private Graphics dbg;
	private Image dbImage = null;
	public boolean colision_izq;
	public boolean colision_der;
	private boolean golpe = false;
	public int vida = 0;
	private boolean golpebajo = false;

	public boolean BANDERA_DE_AVION_DER = false;
	public boolean BANDERA_DE_AVION_IZQ = false;
	// pausa y comienzo
	private BufferedImage rhinoStart;
	private boolean estaEnPausa;
	private boolean estaComenzando;
	int tiempoPausa;
	private boolean irAMenu = false;
	public boolean tryAgain;

	private ImagenesRhino explosionJugador = null;
	private boolean mostrarExplosion = false;
	private int explWidth, explHeight;
	private int xExpl, yExpl;
	private int numHits = 0;

	private EventListenerList eventListenerList = new EventListenerList();

	private ActionListenerProxy actionListenerProxy = new ActionListenerProxy(
			eventListenerList);

	// /////NIVELES
	private Nivel1 n1;
	private Nivel2 n2;
	private Nivel3 n3;
	private Nivel4 n4;
	private Nivel5 n5;

	public RhinoPanel(RhinoDestruction jj, long period, int niv) {
		rhinoTop = jj;
		this.periodo = period;
		nivel = niv;
		tryAgain = false;
		setDoubleBuffered(false);
		setBackground(Color.white);
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
		setFocusable(true);
		addKeyListener(keyl);

		setIrAMenu(false);

		if (nivel == 1) {
			n1 = new Nivel1();
			IMAGENES_INFO = n1.getImagenes_Info();
			VIDA = n1.getVida();
			vidarest = n1.getVidarest();
			PUNT_MAX = n1.getPunt_Max();
			LADRILLOS_INFO = n1.getLadrillos_Info();
			CargadorDeImg imsLoader = new CargadorDeImg(IMAGENES_INFO);
			rhinoStart = imsLoader.getImg("nivel1");
			estaComenzando = true;
			estaEnPausa = true;
			tiempoPausa = 120;

			ladrillin = new ManejadorDeLadrillos(PANEL_WIDTH, PANEL_HEIGHT,
					LADRILLOS_INFO, imsLoader);
			int brickMoveSize = ladrillin.getVelocidad();

			lazin = new ManejadorDeLazo(PANEL_WIDTH, PANEL_HEIGHT,
					brickMoveSize, imsLoader, n1.getFondo1(), n1.getFondo2(),
					n1.getFondo3());
			rhino = new RhinoSprite(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
					ladrillin, imsLoader, (int) (period / 1000000L));
			AVION = new AvionSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			explosionJugador = new ImagenesRhino("explosion",
					(int) (period / 1000000L), 0.5, false, imsLoader);
			BufferedImage explosionIm = imsLoader.getImg("explosion");
			explWidth = explosionIm.getWidth();
			explHeight = explosionIm.getHeight();
			explosionJugador.setWatcher(this);
		}
		if (nivel == 2) {
			n2 = new Nivel2();
			IMAGENES_INFO = n2.getImagenes_Info();
			VIDA = n2.getVida();
			vidarest = n2.getVidarest();
			PUNT_MAX = n2.getPunt_Max();
			LADRILLOS_INFO = n2.getLadrillos_Info();
			pasoMisil = n2.getPasoMisil();
			CargadorDeImg imsLoader = new CargadorDeImg(IMAGENES_INFO);
			rhinoStart = imsLoader.getImg("nivel2");
			estaComenzando = true;
			estaEnPausa = true;
			tiempoPausa = 120;
			ladrillin = new ManejadorDeLadrillos(PANEL_WIDTH, PANEL_HEIGHT,
					LADRILLOS_INFO, imsLoader);
			int brickMoveSize = ladrillin.getVelocidad();
			lazin = new ManejadorDeLazo(PANEL_WIDTH, PANEL_HEIGHT,
					brickMoveSize, imsLoader, n2.getFondo1(), n2.getFondo2(),
					n2.getFondo3());
			rhino = new RhinoSprite(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
					ladrillin, imsLoader, (int) (period / 1000000L));

			misil = new MisilSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino, nivel, pasoMisil);
			AVION = new AvionSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			explosionJugador = new ImagenesRhino("explosion",
					(int) (period / 1000000L), 0.5, false, imsLoader);
			BufferedImage explosionIm = imsLoader.getImg("explosion");
			explWidth = explosionIm.getWidth();
			explHeight = explosionIm.getHeight();
			explosionJugador.setWatcher(this);
		}
		if (nivel == 3) {
			n3 = new Nivel3();
			IMAGENES_INFO = n3.getImagenes_Info();
			VIDA = n3.getVida();
			vidarest = n3.getVidarest();
			PUNT_MAX = n3.getPunt_Max();
			LADRILLOS_INFO = n3.getLadrillos_Info();
			pasoMisil = n3.getPasoMisil();
			CargadorDeImg imsLoader = new CargadorDeImg(IMAGENES_INFO);
			rhinoStart = imsLoader.getImg("nivel3");
			estaComenzando = true;
			estaEnPausa = true;
			tiempoPausa = 120;
			ladrillin = new ManejadorDeLadrillos(PANEL_WIDTH, PANEL_HEIGHT,
					LADRILLOS_INFO, imsLoader);
			int brickMoveSize = ladrillin.getVelocidad();
			lazin = new ManejadorDeLazo(PANEL_WIDTH, PANEL_HEIGHT,
					brickMoveSize, imsLoader, n3.getFondo1(), n3.getFondo2(),
					n3.getFondo3());
			rhino = new RhinoSprite(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
					ladrillin, imsLoader, (int) (period / 1000000L));
			misil = new MisilSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino, nivel, pasoMisil);
			AVION = new AvionSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			COHETE = new CoheteSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader,
					this, rhino, AVION);
			explosionJugador = new ImagenesRhino("explosion",
					(int) (period / 1000000L), 0.5, false, imsLoader);
			BufferedImage explosionIm = imsLoader.getImg("explosion");
			explWidth = explosionIm.getWidth();
			explHeight = explosionIm.getHeight();
			explosionJugador.setWatcher(this);
		}
		if (nivel == 4) {
			n4 = new Nivel4();
			IMAGENES_INFO = n4.getImagenes_Info();
			VIDA = n4.getVida();
			vidarest = n4.getVidarest();
			PUNT_MAX = n4.getPunt_Max();
			LADRILLOS_INFO = n4.getLadrillos_Info();
			pasoMisil = n4.getPasoMisil();
			CargadorDeImg imsLoader = new CargadorDeImg(IMAGENES_INFO);
			rhinoStart = imsLoader.getImg("nivel4");
			estaComenzando = true;
			estaEnPausa = true;
			tiempoPausa = 120;
			ladrillin = new ManejadorDeLadrillos(PANEL_WIDTH, PANEL_HEIGHT,
					LADRILLOS_INFO, imsLoader);
			int brickMoveSize = ladrillin.getVelocidad();
			lazin = new ManejadorDeLazo(PANEL_WIDTH, PANEL_HEIGHT,
					brickMoveSize, imsLoader, n4.getFondo1(), n4.getFondo2(),
					n4.getFondo3());
			rhino = new RhinoSprite(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
					ladrillin, imsLoader, (int) (period / 1000000L));
			misil = new MisilSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino, nivel, pasoMisil);
			AVION = new AvionSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			COHETE = new CoheteSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader,
					this, rhino, AVION);
			tank = new TankSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			tankMisil = new MisilDeTanque(PANEL_WIDTH, PANEL_HEIGHT, imsLoader,
					this, rhino, tank);
			explosionJugador = new ImagenesRhino("explosion",
					(int) (period / 1000000L), 0.5, false, imsLoader);
			BufferedImage explosionIm = imsLoader.getImg("explosion");
			explWidth = explosionIm.getWidth();
			explHeight = explosionIm.getHeight();
			explosionJugador.setWatcher(this);
		}
		if (nivel == 5) {
			n5 = new Nivel5();
			IMAGENES_INFO = n5.getImagenes_Info();
			VIDA = n5.getVida();
			vidarest = n5.getVidarest();
			PUNT_MAX = n5.getPunt_Max();
			LADRILLOS_INFO = n5.getLadrillos_Info();
			pasoMisil = n5.getPasoMisil();
			CargadorDeImg imsLoader = new CargadorDeImg(IMAGENES_INFO);
			rhinoStart = imsLoader.getImg("nivel5");
			estaComenzando = true;
			estaEnPausa = true;
			tiempoPausa = 120;
			ladrillin = new ManejadorDeLadrillos(PANEL_WIDTH, PANEL_HEIGHT,
					LADRILLOS_INFO, imsLoader);
			int brickMoveSize = ladrillin.getVelocidad();
			lazin = new ManejadorDeLazo(PANEL_WIDTH, PANEL_HEIGHT,
					brickMoveSize, imsLoader, n5.getFondo1(), n5.getFondo2(),
					n5.getFondo3());
			rhino = new RhinoSprite(PANEL_WIDTH, PANEL_HEIGHT, brickMoveSize,
					ladrillin, imsLoader, (int) (period / 1000000L));
			misil = new MisilSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino, nivel, pasoMisil);
			misil2 = new MisilSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader,
					this, rhino, nivel, n5.getPasoMisil2());
			AVION = new AvionSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			COHETE = new CoheteSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader,
					this, rhino, AVION);
			tank = new TankSprite(PANEL_WIDTH, PANEL_HEIGHT, imsLoader, this,
					rhino);
			tankMisil = new MisilDeTanque(PANEL_WIDTH, PANEL_HEIGHT, imsLoader,
					this, rhino, tank);
			explosionJugador = new ImagenesRhino("explosion",
					(int) (period / 1000000L), 0.5, false, imsLoader);
			BufferedImage explosionIm = imsLoader.getImg("explosion");
			explWidth = explosionIm.getWidth();
			explHeight = explosionIm.getHeight();
			explosionJugador.setWatcher(this);
		}
		msgFont = new Font("SansSerif", Font.BOLD, 15);
		msgFontGO = new Font("SansSerif", Font.BOLD, 18);
		msgFontPaused = new Font("SansSerif", Font.BOLD, 22);
		metrics = this.getFontMetrics(msgFont);
	}

	KeyListener keyl = new KeyListener() {
		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (!gameOver && !ganador) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					rhino.sinMovimiento();
					ladrillin.quieto();
					lazin.moverNo();
					BANDERA_DE_AVION_IZQ = false;
					break;
				case KeyEvent.VK_RIGHT:
					rhino.sinMovimiento();
					ladrillin.quieto();
					lazin.moverNo();
					BANDERA_DE_AVION_DER = false;
					break;
				case KeyEvent.VK_SPACE:
					rhino.golpe = false;
					break;
				}
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_P) {
				if (estaComenzando || estaEnPausa) {
					estaComenzando = false;
					estaEnPausa = false;
				} else {
					// estaComenzando = true;
					estaEnPausa = true;
					// tiempoPausa=(tiempoPausa - tiempoEnJuego);
				}
			}
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
				if (estaComenzando) {
					stopGame();
				}
			}

			if (!estaComenzando || !estaEnPausa) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					if (!gameOver && !ganador) {
						rhino.movIzquierda();
						ladrillin.moverDerecha();
						lazin.moverDerecha();
						if (colision_der) {
							colision_der = false;
						}
					}
					BANDERA_DE_AVION_IZQ = true;
					break;
				case KeyEvent.VK_RIGHT:
					if (!gameOver && !ganador) {
						rhino.movDerecha();
						ladrillin.moverIzquierda();
						lazin.moverIzquierda();
						if (colision_izq) {
							colision_izq = false;
						}
					}
					BANDERA_DE_AVION_DER = true;
					break;
				case KeyEvent.VK_UP:
					if (!gameOver && !ganador) {
						rhino.salto();
						colision_der = false;
						colision_izq = false;
					}
					break;
				case KeyEvent.VK_SPACE:
					if (!gameOver && !ganador) {
						rhino.golpe();
						golpe = true;
					}
					break;
				case KeyEvent.VK_DOWN:
					if (!gameOver && !ganador) {
						rhino.golpebajo();
						golpebajo = true;
					}
					break;
				case KeyEvent.VK_ESCAPE:
					if (gameOver || ganador || estaEnPausa) {
						rhinoTop.tryAgain = false;
						stopGame();
						// salir = true;
						// restartGame();
					}
					break;
				case KeyEvent.VK_ENTER:
					if (ganador) {
						rhinoTop.tryAgain = false;
						restartGame();
					}
				case KeyEvent.VK_R:
					if (gameOver || estaEnPausa) {
						rhinoTop.tryAgain = true;
						reiniciarNivel();
					}
					break;
				}
			}
		}
	};

	public void addNotify() {
		super.addNotify();
		startGame();
	}

	private void startGame() {
		if (animator == null || !running) {
			animator = new Thread(this);
			animator.start();
		}
	}

	private void restartGame() {
		running = false;
		animator = null;
		rhinoTop.tryAgain = false;
		if (nivel == 1) {
			rhinoTop.nvl2 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 2) {
			rhinoTop.nvl3 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 3) {
			rhinoTop.nvl4 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 4) {
			rhinoTop.nvl5 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
	}

	private void reiniciarNivel() {
		running = false;
		animator = null;
		rhinoTop.tryAgain = true;
		if (nivel == 1) {
			rhinoTop.nvl1 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 2) {
			rhinoTop.nvl2 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 3) {
			rhinoTop.nvl3 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 4) {
			rhinoTop.nvl4 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
		if (nivel == 5) {
			rhinoTop.nvl5 = true;
			ActionEvent evt = new ActionEvent(this, 0, null);
			getActionListenerProxy().fireActionEvent(evt);
		}
	}

	public void renaudarJuego() {
		if (!estaComenzando) {
			estaEnPausa = false;
		}
	}

	public void pausarJuego() {
		estaEnPausa = true;
	}

	public void stopGame() {
		running = false;
		animator = null;
		if (!tryAgain) {
			setIrAMenu(true);
		}
		ActionEvent evt = new ActionEvent(this, 0, null);
		getActionListenerProxy().fireActionEvent(evt);
	}

	public void run() {
		long beforeTime, afterTime, timeDiff, sleepTime;
		long overSleepTime = 0L;
		int noDelays = 0;
		long excess = 0L;
		iniciaTiempoEnJuego = System.currentTimeMillis();
		gameStartTime = J3DTimer.getValue();
		// gameStartTime = System.currentTimeMillis();
		beforeTime = gameStartTime;

		running = true;

		while (running) {
			gameUpdate();
			gameRender();
			paintScreen();

			afterTime = J3DTimer.getValue();
			timeDiff = afterTime - beforeTime;
			sleepTime = (periodo - timeDiff) - overSleepTime;

			if (sleepTime > 0) {
				try {
					Thread.sleep(sleepTime / 100000000L);
				} catch (InterruptedException ex) {
				}
				overSleepTime = (J3DTimer.getValue() - afterTime) - sleepTime;
			} else {
				excess -= sleepTime;
				overSleepTime = 0L;

				if (++noDelays >= NO_DELAYS_PER_YIELD) {
					Thread.yield();
					noDelays = 0;
				}
			}

			beforeTime = J3DTimer.getValue();
			int skips = 0;
			while ((excess > periodo) && (skips < MAX_FRAME_SKIPS)) {
				excess -= periodo;
				gameUpdate();
				skips++;
			}
		}
		if (salir)
			System.exit(0);
	}

	private void gameUpdate() {
		if (!estaEnPausa) {
			if (!gameOver && !ganador) {
				if (rhino.willHitBrick()) {
					rhino.sinMovimiento();
					ladrillin.quieto();
					lazin.moverNo();
				}
				if (golpe) {
					rhino.destruction();
				}
				if (golpebajo) {
					rhino.downdestruction();
				}
				golpebajo = false;
				golpe = false;
				lazin.actualizar();
				ladrillin.update();
				rhino.updateRhinoSprite();
				AVION.updateAvionSprite();
				if (nivel == 4 || nivel == 5)
					tank.updateTankSprite();
				if (nivel != 1)
					misil.updateMisilSprite();
				if (nivel == 3)
					COHETE.updateCoheteSprite();
				if (nivel == 4) {
					// misil2.updateMisilSprite();
					COHETE.updateCoheteSprite();
					tankMisil.updateMisilDeTanque();
				}
				if (nivel == 5) {
					misil2.updateMisilSprite();
					COHETE.updateCoheteSprite();
					tankMisil.updateMisilDeTanque();
				}
				if (mostrarExplosion)
					explosionJugador.updateTick(); // update the animation
				if (gameOver)
					rhino.muerto();
			} else {

			}
		}
	}

	private void gameRender() {
		if (dbImage == null) {
			dbImage = createImage(PANEL_WIDTH, PANEL_HEIGHT);
			if (dbImage == null) {
				// System.out.println("dbImage is null");
				return;
			} else
				dbg = dbImage.getGraphics();
		}
		dbg.setColor(Color.white);
		dbg.fillRect(0, 0, PANEL_WIDTH, PANEL_HEIGHT);

		lazin.display(dbg);
		ladrillin.display(dbg);
		rhino.dibujarSprite(dbg);
		AVION.dibujarSprite(dbg);
		if (nivel != 1)
			misil.dibujarSprite(dbg);
		if (nivel == 1)
			contadorTiempo(dbg);
		if (nivel == 3)
			COHETE.dibujarSprite(dbg);
		if (nivel == 4) {
			tank.dibujarSprite(dbg);
			COHETE.dibujarSprite(dbg);
			tankMisil.dibujarSprite(dbg);
		}
		if (nivel == 5) {
			tank.dibujarSprite(dbg);
			misil2.dibujarSprite(dbg);
			COHETE.dibujarSprite(dbg);
			tankMisil.dibujarSprite(dbg);
		}
		vidaYPuntaje(dbg);
		if (mostrarExplosion)
			dbg.drawImage(explosionJugador.getCurrentImage(), xExpl, yExpl,
					null);
		if (gameOver) {
			rhino.muerto();
			mensajeGameOver(dbg);
		}
		if (ganador)
			mensajeGanador(dbg);

		if (estaEnPausa && !estaComenzando && !ganador & !gameOver)
			mensajePausa(dbg);

		if (estaComenzando) {
			dbg.drawImage(rhinoStart,
					(PANEL_WIDTH - rhinoStart.getWidth()) / 2,
					(PANEL_HEIGHT - rhinoStart.getHeight()) / 2, null);
		}

	}

	private void paintScreen() {
		Graphics g;
		try {
			g = this.getGraphics();
			if ((g != null) && (dbImage != null))
				g.drawImage(dbImage, 0, 0, null);

			Toolkit.getDefaultToolkit().sync();
			g.dispose();
		} catch (Exception e) {
			// System.out.println("Error de Contexto Grafico: " + e);
		}
	}

	private void contadorTiempo(Graphics g) {
		if (nivel == 1) {
			if (!gameOver && !ganador && !estaEnPausa)
				tiempoEnJuego = (int) ((System.currentTimeMillis() - iniciaTiempoEnJuego) / 1000L);
			g.setFont(msgFont);
			g.drawString("Tiempo Restante: " + (tempmax - tiempoEnJuego)
					+ " segs", 10, 20);
			if (tiempoEnJuego == tempmax) {
				gameOver = true;
			}
		}
	}

	private void vidaYPuntaje(Graphics g) {
		g.setColor(Color.white);
		g.setFont(msgFont);
		if (nivel != 1 && vidarest >= 0) {
			if (vidarest == 100)
				g.drawString("Vida Restante: " + vidarest, 10, 20);
			else if (vidarest < 100) {
				g.drawString("Vida Restante: " + (vidarest + ladrillin.vida), 10, 20);
			}
		} else if (vidarest <= 0)
			g.drawString("Vida Restante: 0", 10, 20);
		if ((vidarest+ladrillin.vida) <= 0)
			gameOver = true;
		if (ladrillin.puntaje <= PUNT_MAX)
			g.drawString("Puntaje: " + ladrillin.puntaje + "/" + PUNT_MAX, 10,
					40);
		else
			g.drawString("Puntaje: " + PUNT_MAX + "/" + PUNT_MAX, 10, 40);
		if (ladrillin.puntaje >= PUNT_MAX)
			ganador = true;
		if (!estaEnPausa && !gameOver)
			g.drawString("Pausa P", PANEL_WIDTH - 80, 20);
	}

	private void mensajeGanador(Graphics g) {
		if (nivel == 1 || nivel == 2 || nivel == 3 || nivel == 4) {
			String msg = "Felicidades has completado el nivel!";
			String msg2 = "Presiona ESC para volver al menu o";
			String msg3 = "ENTER para pasar al Siguiente Nivel";
			int x = (PANEL_WIDTH - metrics.stringWidth(msg)) / 2;
			int x1 = (PANEL_WIDTH - metrics.stringWidth(msg2)) / 2;
			int x2 = (PANEL_WIDTH - metrics.stringWidth(msg3)) / 2;
			int y = (PANEL_HEIGHT - metrics.getHeight()) / 3;
			g.setColor(Color.white);
			g.setFont(msgFontGO);
			g.drawString(msg, x - 38, y);
			g.drawString(msg2, x1 - 38, y + 27);
			g.drawString(msg3, x2 - 38, y + 48);
		}
		if (nivel == 5) {
			String msg = "FELICIDADES HAS COMPLETADO EL JUEGO!";
			String msg2 = "Presiona ESC para volver al menu";
			int x = (PANEL_WIDTH - metrics.stringWidth(msg)) / 2;
			int x1 = (PANEL_WIDTH - metrics.stringWidth(msg2)) / 2;
			int y = (PANEL_HEIGHT - metrics.getHeight()) / 3;
			g.setColor(Color.RED);
			g.setFont(msgFontGO);
			g.drawString(msg, x, y);
			g.drawString(msg2, x1, y + 18);
		}
	}

	private void mensajeGameOver(Graphics g) {
		String msg = "Juego Terminado.";
		String msg1 = "Presiona R para reiniciar o";
		String msg2 = "ESC para salir.";
		int x = (PANEL_WIDTH - metrics.stringWidth(msg)) / 2;
		int x1 = (PANEL_WIDTH - metrics.stringWidth(msg1)) / 2;
		int x2 = (PANEL_WIDTH - metrics.stringWidth(msg2)) / 2;
		int y = (PANEL_HEIGHT - metrics.getHeight()) / 3;
		g.setColor(Color.white);
		g.setFont(msgFontGO);
		g.drawString(msg, x, y);
		g.drawString(msg1, x1, y + 27);
		g.drawString(msg2, x2, y + 48);
	}

	private void mensajePausa(Graphics g) {
		String msg = "PAUSA";
		String msg2 = "Presiona P para renaudar,";
		String msg3 = "R para reiniciar o";
		String msg4 = "ESC para volver al menu.";
		int x = (PANEL_WIDTH - metrics.stringWidth(msg)) / 2;
		int x1 = (PANEL_WIDTH - metrics.stringWidth(msg2)) / 2;
		int x2 = (PANEL_WIDTH - metrics.stringWidth(msg3)) / 2;
		int x3 = (PANEL_WIDTH - metrics.stringWidth(msg4)) / 2;
		int y = (PANEL_HEIGHT - metrics.getHeight()) / 3;
		g.setColor(Color.white);
		g.setFont(msgFontPaused);
		g.drawString(msg, x, y);
		g.drawString(msg2, x1 - 38, y + 27);
		g.drawString(msg3, x2 - 38, y + 48);
		g.drawString(msg4, x3 - 38, y + 69);
	}

	private void btnRhinoPanelClicked() {
		ActionEvent evt = new ActionEvent(this, 0, null);
		getActionListenerProxy().fireActionEvent(evt);
	}

	public ActionListenerProxy getActionListenerProxy() {
		return actionListenerProxy;
	}

	public void explosion(int x, int y) {
		if (!mostrarExplosion) {
			mostrarExplosion = true;
			xExpl = x - explWidth / 2;
			yExpl = y - explHeight / 2;
			// clipsLoader.play(exploNames[numHits % exploNames.length], false);
			numHits++;
			// vida = numHits * 5;
		}
	}

	public void sequenceEnded(String imageName) {
		mostrarExplosion = false;
		explosionJugador.restartAt(0);
//		vidarest = VIDA - vida;
//		if (vidarest == 0) {
//			gameOver = true;
//			// clipsLoader.play("applause", false);
//		}
	}

	public void explosionCohete(int x, int y) {
		if (!mostrarExplosion) {
			mostrarExplosion = true;
			xExpl = x;
			yExpl = y;
			// clipsLoader.play(exploNames[numHits % exploNames.length], false);
		}
	}

	public void destruirAvion(int x, int y) {
		if (!mostrarExplosion) {
			mostrarExplosion = true;
			xExpl = x - explWidth / 2;
			yExpl = y - explHeight / 2;
			// clipsLoader.play(exploNames[numHits % exploNames.length], false);
			ladrillin.puntaje += 5;
		}
	}

	public void comerHombre(int x, int y) {
		xExpl = x - explWidth / 2;
		yExpl = y - explHeight / 2;
		// clipsLoader.play(exploNames[numHits % exploNames.length], false);
		vidarest += 5;
	}

	public boolean isIrAMenu() {
		return irAMenu;
	}

	public void setIrAMenu(boolean irAMenu) {
		this.irAMenu = irAMenu;
	}

	public boolean isTryAgain() {
		return tryAgain;
	}
}
