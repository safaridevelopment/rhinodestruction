package Wapay;

import java.awt.*;
import java.awt.event.*;


import javax.swing.*;



public class Board extends JPanel implements ActionListener, KeyListener {
	
	Wapay p;
	Image img;
	Timer time;

	public Board() {
		p = new Wapay();
		
		
		addKeyListener(this);
		setFocusable(true);
		ImageIcon i = new ImageIcon ("/home/xavier/workspace/SideScroller/background.png");
		img = i.getImage();
		time= new Timer(5,this);
		time.start();
		
	}
	
	public void actionPerformed(ActionEvent e) {
		p.move();
		repaint();
	}
	
	public void paint (Graphics g) {
		super.paint(g);
		Graphics2D g2d= (Graphics2D) g;
		int borde = 3100;
		
		if((p.getX() - borde) % 8040 == 0){
			p.frame2=0;
		}
		if((p.getX() - borde+4100) % 8040 ==0){
			p.frame1 =0;
		}
		
		
		g2d.drawImage(img, 1050-p.frame1,0,null);
		if(p.getX() >= borde){	
			g2d.drawImage(img, 1050-p.frame2,0,null);
		}
		System.out.println(p.getX());
		g2d.drawImage(p.getImage(), p.left_restriction, p.getY(),null);
						
	}
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_LEFT ) {
			p.dx= -p.speed;
			p.still=p.left_running.getImage();
			//System.out.println("Izquierda");
		}
		
		if(key == KeyEvent.VK_RIGHT) {
			p.dx=p.speed;
			p.still=p.right_running.getImage();
			//System.out.println("Derecha");
		}	
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_LEFT) {
			p.dx= 0;
			p.still=p.left_still.getImage();
		}
		
		if(key == KeyEvent.VK_RIGHT) {
			p.dx=0;
			p.still=p.right_still.getImage();
		}
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
		
	}
	
	


