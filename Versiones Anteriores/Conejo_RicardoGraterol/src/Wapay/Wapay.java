package Wapay;

import java.awt.*;
import java.awt.event.*;

import javax.swing.ImageIcon;

public class Wapay {
	int x, dx, y, frame1, frame2, left_restriction, speed;
	Image still;
	ImageIcon right_running = new ImageIcon(
			"/home/xavier/workspace/SideScroller/bunny.gif");
	ImageIcon left_running = new ImageIcon(
			"/home/xavier/workspace/SideScroller/bunnyleft.gif");
	ImageIcon right_still = new ImageIcon(
			"/home/xavier/workspace/SideScroller/1.png");
	ImageIcon left_still = new ImageIcon(
			"/home/xavier/workspace/SideScroller/3.png");

	public Wapay() {

		still = right_still.getImage();
		left_restriction = 200;
		x = 100;
		frame1 = 1050;
		frame2 = 0;
		y = 288;
		speed=5;

	}

	public void move() {

		if (dx != -speed) {
			if (left_restriction + dx <= 700) {
				left_restriction = left_restriction + dx;
			} else {
				x = x + dx;
				frame1 = frame1 + dx;
				frame2 = frame2 + dx;
			}
		}
		else {
			if(left_restriction + dx >0 )
				left_restriction = left_restriction +dx;
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Image getImage() {
		return still;
	}

}
